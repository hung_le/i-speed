//
//  MoreVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/19/21.
//

import UIKit

class MoreVC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //setGradientBackground()
    }
    
    @IBAction func intro(_ sender: Any) {
        let vc = IntroVC()
        vc.id = 1
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func update(_ sender: Any) {
        let vc = IntroVC()
        vc.id = 2
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func protect(_ sender: Any) {
        let vc = IntroVC()
        vc.id = 3
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func quyen(_ sender: Any) {
        let vc = IntroVC()
        vc.id = 4
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func codeIntroduce(_ sender: Any) {
        let vc = CodeIntroVC()
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
