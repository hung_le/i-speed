//
//  PopupIPVC.swift
//  I-Speed
//
//  Created by mac on 16/09/2023.
//

import UIKit
import CoreTelephony
import FGRoute

class PopupDeviceVC: UIViewController {
    var deviceNames: String?
    var SSSD: String?
    var locations: String?

    @IBOutlet weak var deviceName: UILabel!
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var ssidDe: UILabel!
    @IBOutlet weak var osVer: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let netInfo = CTTelephonyNetworkInfo()
        let carrier = netInfo.serviceSubscriberCellularProviders?.filter({ $0.value.carrierName != nil }).first?.value
        
        if SSSD == "WIFI" {
            ssidDe.text = FGRoute.getSSID()
        } else {
            ssidDe.text = carrier?.carrierName
        }
        deviceName.text = deviceNames
        location.text = UserDefaults.standard.value(forKey: "CITY") as? String//.set(pointCheckSend?.city, forKey: "CITY")
        osVer.text = getOSInfo()
        
    }
    
    func getOSInfo()->String {
        let os = ProcessInfo.processInfo.operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
