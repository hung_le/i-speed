//
//  TokenModel.swift
//  I-Speed
//
//  Created by Le Hung on 3/13/21.
//

import Foundation
import Alamofire

struct TokenModel: Decodable {
    var access_token: String?
    var token_type: String?
    var expires_in: Int?
    var scope: String?
    var jti: String?
    
    
    enum CodingKeys: String, CodingKey {
        case access_token = "access_token"
        case token_type = "token_type"
        case expires_in = "expires_in"
        case scope = "scope"
        case jti = "jti"
        
    }
}

