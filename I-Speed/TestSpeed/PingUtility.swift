//
//  PingUnitity.swift
//  I-Speed
//
//  Created by Mobifone IT Ptpm on 4/7/24.
//

import Foundation

class PingUtility {
    private let url: URL
    private var attemptPings: Int
    private var attemptPing: Int = 0
    private var pings: [Double] = []
    private let session = URLSession(configuration: .default)

    init(url: URL, attempts: Int = 3) {
        self.url = url
        self.attemptPings = attempts
    }

    func ping(completion: @escaping (Double?, Double?, Error?) -> Void) {
        attemptPing += 1
        let startTime = Date()

        let task = session.dataTask(with: url) { _, response, error in
            let endTime = Date()
            if let error = error {
                completion(nil, nil, error)
                return
            }

            let pingTime = endTime.timeIntervalSince(startTime) * 1000 // Convert to milliseconds
            self.pings.append(pingTime)
            
            if self.attemptPing < self.attemptPings {
                self.ping(completion: completion)
            } else {
                let averagePing = self.pings.reduce(0, +) / Double(self.pings.count)
                let jitter = self.calculateJitter(pings: self.pings)
                completion(averagePing, jitter, nil)
            }
        }
        
        task.resume()
    }

    private func calculateJitter(pings: [Double]) -> Double {
        let meanPing = pings.reduce(0, +) / Double(pings.count)
        let squaredDifferences = pings.map { ($0 - meanPing) * ($0 - meanPing) }
        let variance = squaredDifferences.reduce(0, +) / Double(pings.count)
        return sqrt(variance)
    }
}
