//
//  ServerCell.swift
//  I-Speed
//
//  Created by Le Hung on 3/26/21.
//

import UIKit

class ServerCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var nameServer: UILabel!
    @IBOutlet weak var cityServer: UILabel!
    @IBOutlet weak var distanceServer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContent.clipsToBounds = true
        viewContent.layer.cornerRadius = 8
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
