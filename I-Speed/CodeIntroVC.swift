//
//  CodeIntroVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/28/22.
//

import UIKit
import Alamofire
import CoreTelephony
import Foundation
import SwiftyJSON
import Network
import SVProgressHUD

class CodeIntroVC: UIViewController {
    @IBOutlet weak var enterCode: UITextField!
    
    @IBOutlet weak var titleCode: UILabel!
    @IBOutlet weak var nameOr: UILabel!
    
    @IBOutlet weak var lbCode: UILabel!
    @IBOutlet weak var lbNameOr: UILabel!
    @IBOutlet weak var Send: UIButton!
    @IBOutlet weak var stactView: UIStackView!
    @IBOutlet weak var checkCode: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dismissKeyboard()
        //self.enterCode.searchBarStyle = UISearchBar.Style.prominent
        //self.enterCode.isTranslucent = false
        enterCode?.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.01960784314, blue: 0.2235294118, alpha: 1)
        enterCode?.layer.borderWidth = 1
        enterCode?.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        enterCode?.layer.cornerRadius = 22
        
        checkCode.layer.cornerRadius = 22
        checkCode.clipsToBounds = true
        checkCode.layer.borderWidth = 1
        checkCode.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        Send.layer.cornerRadius = 22
        Send.clipsToBounds = true
        Send.layer.borderWidth = 1
        Send.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        //self.enterCode.barTintColor = #colorLiteral(red: 0.2008568347, green: 0.05059870332, blue: 0.2883019149, alpha: 1)
        let isIntro = UserDefaults.standard.value(forKey: "isIntro")
        if((isIntro != nil) == true) {
            guard let key = UserDefaults.standard.value(forKey: "name") as? String else {return}
            lbCode.text = "Mã giới thiệu: \(key)"
            guard let key2 = UserDefaults.standard.value(forKey: "name123") as? String else {return}
            lbNameOr.text =  "Tên tổ chức: \(key2)"
            stactView.isHidden = false
            Send.isHidden = true
            checkCode.isHidden = true
            enterCode.isHidden = true
        } else {
            enterCode.attributedPlaceholder =
            NSAttributedString(string: "Nhập mã giới thiệu của bạn", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            enterCode.font =  UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.thin)
            stactView.isHidden = true
            Send.isHidden = true
            enterCode.isHidden = false
            checkCode.isHidden = false
            
        }
        
    }
    @IBAction func checkCode(_ sender: Any) {
        self.getCode(codeIntro: self.enterCode.text)
        
    }
    
    @IBAction func back_action(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendCode(codeIntro: String) {
        if let clientCode = UserDefaults.standard.value(forKey:"CLIENT") as? String,
           let token = UserDefaults.standard.value(forKey: "TOKEN") as? String {
            let header: HTTPHeaders = [.authorization(bearerToken: token)]
            let param: [String: String] = [
                "referralCode": codeIntro ,
                "clientCode": clientCode,
                
            ]
            print("header: \(header)")
            print("param: \(param)")
            //SVProgressHUD.show()
            AF.request("https://api.i-speed.vn/v3/referrals" , method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseJSON { [weak self] (response) in
                switch response.result {
                case .success(_):
                    //SVProgressHUD.dismiss()
                    let json = try? JSON(data: response.data!)
                    let name = json?["status"].stringValue
                    if (name == "SUCCESS") {
                        DispatchQueue.main.async {
                            self?.enterCode.isHidden = true
                            self?.Send.isHidden = true
                            self?.stactView.isHidden = false
                            UserDefaults.standard.set(true , forKey: "isIntro")
                            self?.showAlertWarning(title: "Nhập mã thành công", content: "Bạn đã nhập mã giới thiệu thành công!")
                        }
                    }
                case .failure(_):
                    //SVProgressHUD.dismiss()
                    self?.showAlertWarning(title: "Sai mã giới thiệu", content: "Vui lòng kiểm tra lại mã giới thiệu")
                    
                }
            }
            
        }
    }
        
    
    
    
    func showAlertWarning(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel) {[weak self] (_) -> Void in
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getCode(codeIntro: String!) {
        //SVProgressHUD.show()
        let urlPoint = "https://api.i-speed.vn/v3/referrals"
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        print(token as Any)
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        let param: [String: String] = [
            "code": codeIntro,
            
        ]
        print("param: \(param)")
        AF.request(urlPoint , method: .get, parameters: param as Parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { [weak self] (response) in
            switch response.result {
                
            case .success(_):
                //SVProgressHUD.dismiss()
                guard let dataRes = response.data else {
                    return
                }
                let json = try? JSON(data: dataRes)
                let valid = json?["available"].boolValue
                if(valid == true) {
                    if let name = json?["referralCode"].stringValue  {
                        UserDefaults.standard.set(name , forKey: "name")
                        self?.lbCode.text = "Mã giới thiệu: \(name)"
                    }
                    if let name1 = json?["orgName"].stringValue {
                        UserDefaults.standard.set(name1 , forKey: "name123")
                        self?.lbNameOr.text = "Tên tổ chức: \(name1)"
                    }
                    
                    DispatchQueue.main.async {
                        self?.stactView.isHidden = false
                        self?.Send.isHidden = false
                        self?.checkCode.isHidden = true
                        self?.lbCode.isHidden = false
                        self?.lbNameOr.isHidden = false
                    }
                    
                    print("json:\(json ?? "")")
                } else {
                    
                    self?.showAlertWarning(title: "Mã không hợp lệ", content: "Vui lòng kiểm tra lại mã giới thiệu")
                }
                
                
                
            case .failure(_):
                //SVProgressHUD.dismiss()
                break
                
            }
        }
    }
    
    @IBAction func sendCode(_ sender: Any) {
        if(enterCode.text != nil) {
            self.sendCode(codeIntro: enterCode.text!)
        }
    }
}
