//
//  HomeViewController.swift
//  I-Speed
//
//  Created by Le Hung on 3/3/21.
//

import UIKit
import Alamofire
import FGRoute
import SystemConfiguration
import CoreTelephony
import Foundation
import SystemConfiguration.CaptiveNetwork
import CoreLocation
import NetworkExtension
import DropDown
import SwiftyJSON
import Network
import SVProgressHUD
import SwiftyGif
import PullToRefresh

enum ConnectionType {
    case wifi
    case cellular
    case ethernet
    case unknown
}



class HomeViewController: UIViewController, CLLocationManagerDelegate, SendDataDelegate, URLSessionDelegate {
    
    func sendData(pointCheck: PointCheckModel) {
        self.pointCheckSend = pointCheck
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .lightContent
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let latestLocation = locations.last else { return }
        
        // Update the label with the new location
        self.lat = String(latestLocation.coordinate.latitude)
        self.long = String(latestLocation.coordinate.longitude)
        self.getNameCity(lat: latestLocation.coordinate.latitude, long: latestLocation.coordinate.longitude)
        // Stop updating location if you only need one update
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    var connectionType: ConnectionType = .unknown
    
    let serialQueue = DispatchQueue(label: "myResourceQueue")
    var hostsot: NEHotspotNetwork!
    
    var listDataPing = [PointCheckModel]()
    var pointCheck: [PointCheckModel]? {
        didSet {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                //                self.nameLocationCity.text = self.pointCheck?.first?.city
            }
            
        }
    }
    lazy var newDataArr:[PointCheckModel] = [] {
        didSet {
            self.newDataSet = self.newDataArr.filter({$0.pingValue != 0}).sorted { $0.pingValue < $1.pingValue }
        }
    }
    
    var isTypeNetwork = ""
    var isCallDelegate: Bool = false
    lazy var newDataSet: [PointCheckModel] = [] {
        didSet {
            if newDataSet.count > 5 {
                DispatchQueue.main.async {
                    self.nameLocation.fadeTransition(0.4)
                    self.nameLocation.text = self.newDataSet.first?.city
                    self.nameLocationTest.fadeTransition(0.4)
                    self.nameLocationTest.text = self.newDataSet.first?.name
                    self.imgLoading.isHidden = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if self.nameLocation.text != nil && self.nameLocationTest.text != nil {
                            self.btnShow.isHidden = false
                            self.btnTestSpeed.isEnabled = true
                        }
                    }
                }
            }
        }
    }
    
    var pointCheckSend: PointCheckModel?
    {
        didSet {
            nameLocationTest.text = pointCheckSend?.name
            nameLocation.text = pointCheckSend?.city
        }
    }
    
    var scrollViewRe: UIScrollView!
    var refreshControl: UIRefreshControl!
    let dropDown = DropDown()
    
    @IBOutlet weak var imgLoadingName: UIImageView!
    var locationManager = CLLocationManager()
    let queueNetWork = DispatchQueue(label: "NetworkMonitor")
    let monitor = NWPathMonitor()
    
    var currentLocation: CLLocation!
    var lat: String? {
        didSet {
            UserDefaults.standard.set(self.lat, forKey: "LATITUDE")
            UserDefaults.standard.set(self.lat, forKey: "latitude")
        }
    }
    var long: String? {
        didSet {
            UserDefaults.standard.set(self.long, forKey: "LONGITUDE")
            UserDefaults.standard.set(self.long, forKey: "longitude")
            
        }
    }
    var ipV4: String = "" {
        didSet {
            if ipV4 != "" {
                isSupportV4 = true
            } else {
                isSupportV4 = false
            }
        }
    }
    var ipV6: [IPModel] = [] {
        didSet {
            if ipV6.count > 0 && isSupportV4 {
                DispatchQueue.main.async {
                    self.statusIPv6.textColor = .white
                    self.StatusIPv4.textColor = .white
                    self.btnIPv6.isEnabled = true
                    self.btnIPv6.isEnabled = true
                    
                }
            }
        }
        
    }
    var isSupportV4 = false
    let manager = NetworkReachabilityManager(host: "www.google.com")
    var token: TokenModel?
    var ip: IPModel?
    var imei: String?
    var jitter = 0.0
    var ping = 0.0
    var pingArr:[Double]? = []
    var pingResult: [Double]?
    
    @IBOutlet weak var imgBackgroundHome: UIImageView!
    @IBOutlet weak var viewlineAround: UIView!
    private var attemptPings = 0
    private var attemptPing = 1
    private var startPingHome: TimeInterval!
    private var sessionPingHome: URLSession!
    private var taskPingHome: URLSessionDataTask!
    private let queuePingHome = OperationQueue()
    var baseUrlString: String?
    var pingUrlString: String?
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var spinderLoading: UIActivityIndicatorView!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var ipAdress: UILabel!
    @IBOutlet weak var nameNetwork: UILabel!
    @IBOutlet weak var typeConnect: UILabel!
    @IBOutlet weak var nameDevices: UILabel!
    @IBOutlet weak var nameLocation: UILabel!
    @IBOutlet weak var IPImage: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var lbTitleSSID: UILabel!
    @IBOutlet weak var nameThietBi: UILabel!
    @IBOutlet weak var osDevices: UILabel!
    
    @IBOutlet weak var statusIPv6: UILabel!
    @IBOutlet weak var StatusIPv4: UILabel!
    @IBOutlet weak var nameLocationCity: UILabel!
    @IBOutlet weak var SSIDNetWork: UILabel!
    
    @IBOutlet weak var btnIPv6: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imgLoading: UIImageView!
    @IBOutlet weak var btnIPv4: UIButton!
    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var viewAni: UIView!
    @IBOutlet weak var viewWaiting: UIView!
    @IBOutlet weak var ImageNetwork: UIImageView!
    @IBOutlet weak var nameLocationTest: UILabel!
    @IBOutlet weak var nameIP: UILabel!
    @IBOutlet weak var btnTestSpeed: UIButton!
    let refresher = PullToRefresh()
    var isComback = false
    var data = ServerVC()
    @IBOutlet weak var viewScrollView : UIView!
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        scrollViewRe.bringSubviewToFront(btnShow)
        //        scrollViewRe.bringSubviewToFront(btnIPv4)
        //        scrollViewRe.bringSubviewToFront(btnIPv4)
        //        scrollViewRe.bringSubviewToFront(btnTestSpeed)
    }
    
    func handleNetworkChange(isConnected: Bool, interfaceType: NWInterface.InterfaceType, isExpensive: Bool) {
        // Xử lý thay đổi mạng ở đây
        if isConnected {
            print("Màn hình 2: Đã kết nối qua \(interfaceType)")
            if interfaceType == .cellular {
                UserDefaults.standard.setValue("cellular", forKey: "NetWork")
                self.connectionType = .cellular
            } else {
                UserDefaults.standard.setValue("wifi", forKey: "NetWork")
                self.connectionType = .wifi
            }
            self.getTokenKey { [weak self] in
                DispatchQueue.main.async {
                    self?.getAllServer()
                    self?.getIPV4FromServer {[weak self] in
                        self?.getIPV6FromServer()
                    }
                    self?.getIconNetwork()
                    self?.btnTestSpeed.isEnabled = true
                    self?.btnShow.isHidden = false
                    self?.ImageNetwork.isHidden = false
                }
            }
        } else {
            print("Màn hình 2: Mất kết nối")
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Mất kết nối", message: "Mất kết nối, vui lòng bật kết nối mạng và thử lại", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.btnTestSpeed.isEnabled = false
                self.btnShow.isHidden = true
                //self.imgLocation.isHidden = true
                self.ImageNetwork.setImage(UIImage(named: "noInter") ?? UIImage())
                self.nameDevices.text = "Không có kết nối internet"
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        scrollViewRe = UIScrollView(frame: view.bounds)
        scrollViewRe.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollViewRe)
        
        // Set up Auto Layout constraints for UIScrollView
        NSLayoutConstraint.activate([
            scrollViewRe.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollViewRe.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollViewRe.topAnchor.constraint(equalTo: view.topAnchor),
            scrollViewRe.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        viewScrollView.addSubview(scrollViewRe)
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Kéo để làm mới")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // Gán refreshControl cho UIScrollView
        scrollViewRe.refreshControl = refreshControl
        btnIPv4.isEnabled = false
        btnIPv6.isEnabled = false
        self.getIconNetwork()
        self.data.delegate = self
        let defaults = UserDefaults.standard
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        // Request location authorization
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            // Configure the location manager
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        self.imei = UIDevice.current.identifierForVendor?.uuidString
        let isFirst: Bool = UserDefaults.standard.bool(forKey: "isFirstTime")
        if !isFirst {
            let uuid = UUID().uuidString
            print(uuid)
            let defaults = UserDefaults.standard
            defaults.set(uuid, forKey: "UUID")
            getTokenKey { [weak self] in
                self?.getInfoFirstTime()
            }
            
        } else {
            let clientCode = UserDefaults.standard.value(forKey:"CLIENT") as? String
            if(clientCode == nil || clientCode == "") {
                let uuid = UUID().uuidString
                print(uuid)
                let defaults = UserDefaults.standard
                defaults.set(uuid, forKey: "UUID")
                getTokenKey { [weak self] in
                    self?.getInfoFirstTime()
                }
            }
        }
        AppUpdater.shared.showUpdate(withConfirmation: true)
        navigationController?.isNavigationBarHidden = true
        UserDefaults.standard.set(UIDevice.modelName, forKey: "DEVICES")
        UserDefaults.standard.set(self.getConnectionType(), forKey: "TYPE_CO")
        UserDefaults.standard.set(self.getConnectionType(), forKey: "networkType")
        defaults.set(true, forKey: "isFirstTime")
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        print("RefreshingRefreshingRefreshing")
        self.imgLoadingName.isHidden = false
        self.imgLoading.isHidden = false
        DispatchQueue.main.async {
            self.SSIDNetWork.text = ""
            self.nameLocationTest.text = ""
            do {
                let gif = try UIImage(gifName: "animate.gif")
                self.imgLoadingName.setGifImage(gif, loopCount: -1)
                self.imgLoading.setGifImage(gif, loopCount: -1)
            } catch {
                print(error)
            }
        }
        self.getTokenKey { [weak self] in
            self?.getAllServer()
            self?.getIconNetwork()
            self?.getIPV4FromServer {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    self?.refreshControl.endRefreshing()
                }
            }
        }
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let ip = FGRoute.getIPAddress() {
            UserDefaults.standard.setValue(ip, forKey: "IP")
            UserDefaults.standard.setValue(ip, forKey: "externalAddress")
        }
        self.getTokenKey { [weak self] in
            self?.getAllServer()
            self?.getIconNetwork()
            self?.getIPV4FromServer {
                self?.getIPV6FromServer()
            }
        }
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        locationManager.stopUpdatingLocation()
        if let address = placemark.name,let city = placemark.locality {
            //self.nameLocationCity.text = "\(city)"
        }
    }
    
    func getIconNetwork() {
        let text = self.getConnectionType()
        self.ImageNetwork.fadeTransition(0.4)
        self.nameDevices.fadeTransition(0.4)
        if text == "LTE" {
            self.ImageNetwork.setImage(UIImage(named: "Cellu")!)
            self.nameDevices.text = "LTE/4G"
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
        }
        if text == "3G" {
            self.ImageNetwork.setImage(UIImage(named: "Cellu")!)
            self.nameDevices.text = "3G"
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
            
        }
        if text == "WIFI" {
            self.ImageNetwork.setImage(UIImage(named: "WIFI")!)
            self.nameDevices.isHidden = false
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
            self.nameDevices.text = FGRoute.getSSID()
        }
        if text == "5G" {
            self.ImageNetwork.setImage(UIImage(named: "Cellu")!)
            self.nameDevices.text = "5G"
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
            
        }
        if text == "2G" {
            self.ImageNetwork.setImage(UIImage(named: "Cellu")!)
            self.nameDevices.text = "2G"
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
            
        }
        if text == "Unknown" {
            self.ImageNetwork.setImage(UIImage(named: "cellular")!)
            self.nameDevices.text = "Unknown"
            self.nameNetwork.isHidden = true
            self.lbTitleSSID.text = "Tên mạng"
        }
        self.nameThietBi.text = UIDevice.modelName
        self.osDevices.text = self.getOSInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sessionPingHome?.invalidateAndCancel()
        taskPingHome?.cancel()
        queuePingHome.isSuspended = true
        self.pointCheckSend = nil
        self.newDataArr.removeAll()
        self.newDataSet.removeAll()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(UIDevice.modelName == "iPhone 6" ||
           UIDevice.modelName == "iPhone 6s" ||
           UIDevice.modelName == "iPhone 7" ||
           UIDevice.modelName == "iPhone 8" ||
           UIDevice.modelName == "iPhone 8 Plus" ||
           UIDevice.modelName == "iPhone 7 Plus" ||
           UIDevice.modelName == "iPhone 6 Plus"
        ) {
            //topContraint.constant = 30
            //bottomContraint.constant = 30
            
        }
        do {
            let gif = try UIImage(gifName: "animate.gif")
            self.imgLoadingName.setGifImage(gif, loopCount: -1)
        } catch {
            print(error)
        }
        locationManager.startUpdatingLocation()
        nameNetwork.text = ""
        nameLocationTest.text = ""
        btnShow.isHidden = true
        btnTestSpeed.isEnabled = false
        do {
            let gif = try UIImage(gifName: "animate.gif")
            self.imgLoading.setGifImage(gif, loopCount: -1)
        } catch {
            print(error)
        }
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        navigationController?.isNavigationBarHidden = true
        btnShow.layer.cornerRadius = 16
        btnShow.clipsToBounds = true
        btnShow.layer.borderWidth = 1
        btnShow.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnTestSpeed.isUserInteractionEnabled = true
        //        monitor.pathUpdateHandler = { path in
        //            if path.status == .satisfied {
        //                if path.usesInterfaceType(.wifi) {
        //                    UserDefaults.standard.setValue("wifi", forKey: "NetWork")
        //                    self.connectionType = .wifi
        //                } else if path.usesInterfaceType(.cellular) {
        //                    UserDefaults.standard.setValue("cellular", forKey: "NetWork")
        //                    self.connectionType = .cellular
        //
        //                }
        //                self.getTokenKey { [weak self] in
        //                    DispatchQueue.main.async {
        //                        self?.getAllServer()
        //                        self?.getIPV4FromServer {[weak self] in
        //                            self?.getIPV6FromServer()
        //                        }
        //                        self?.getIconNetwork()
        //                        self?.btnTestSpeed.isEnabled = true
        //                        self?.btnShow.isHidden = false
        //                        //                        self?.imgLocation.isHidden = false
        //                        self?.ImageNetwork.isHidden = false
        //                    }
        //                }
        //            } else {
        //                DispatchQueue.main.async {
        //                    let alert = UIAlertController(title: "Mất kết nối", message: "Mất kết nối, vui lòng bật kết nối mạng và thử lại", preferredStyle: UIAlertController.Style.alert)
        //                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        //                    self.present(alert, animated: true, completion: nil)
        //                    self.btnTestSpeed.isEnabled = false
        //                    self.btnShow.isHidden = true
        //                    //self.imgLocation.isHidden = true
        //                    self.ImageNetwork.setImage(UIImage(named: "noInter") ?? UIImage())
        //                    self.nameDevices.text = "Không có kết nối internet"
        //                }
        //            }
        //        }
        //        monitor.start(queue: queueNetWork)
        
        NetworkMonitoring.shared.onStatusChange = { [weak self] isConnected, interfaceType, isExpensive in
            self?.handleNetworkChange(isConnected: isConnected, interfaceType: interfaceType, isExpensive: isExpensive)
        }
        
        
    }
    
    func getInfoFirstTime() {
        if let auth = UserDefaults.standard.value(forKey: "UUID") {
            let paramaters : Parameters = [
                "UUID": auth as Any,
                "clientType": "MOBILE",
                "deviceCode": " ",
                "deviceName": UIDevice.modelName,
                "osType": "iOS",
                "osVersion": UIDevice.current.systemVersion,
                "city": "",
                "latitude": self.lat as Any,
                "longitude": self.long as Any,
            ]
            
            let token = UserDefaults.standard.value(forKey: "TOKEN")
            let headers: HTTPHeaders = [.authorization(bearerToken: token as! String)]
            
            AF.request("https://api.i-speed.vn/v3/clients", method: .post, parameters: paramaters, encoding: JSONEncoding.default, headers: headers ).responseData { response in
                if response.error == nil {
                    let json = try? JSON(data: response.data!)
                    let name = json?["message"].stringValue
                    let clientCode = name?.dropFirst(28)
                    let defaults = UserDefaults.standard
                    defaults.set(clientCode, forKey: "CLIENT")
                    defaults.set(clientCode, forKey: "clientCode")
                    print(clientCode as Any)
                }
            }
            
        }
        
    }
    
    @IBAction func onIPv6(_ sender: Any) {
        let vc = PopupIPV6VC()
        vc.ipType = "IPv6"
        vc.ip = self.ipV6
        self.present(vc, animated: true)
    }
    @IBAction func onDevice(_ sender: Any) {
        let vc = PopupDeviceVC()
        vc.deviceNames = UIDevice.modelName
        vc.locations = self.pointCheckSend?.city
        vc.SSSD = self.getConnectionType()
        
        self.present(vc, animated: true)
    }
    @IBAction func onIPv4(_ sender: Any) {
        let vc = PopupIPVC()
        vc.ip = self.ipV4
        vc.ipType = "IPv4"
        self.present(vc, animated: true)
    }
    @IBAction func testSpeed_tap(_ sender: Any) {
        print("tap")
        if self.pointCheckSend == nil {
            pointCheckSend = newDataSet.first
        }
        //monitor.cancel()
        UserDefaults.standard.set(pointCheckSend?.name, forKey: "NAME_NET")
        UserDefaults.standard.set(pointCheckSend?.city, forKey: "CITY")
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let vc = SpeedTestVC()
            vc.ipAdress = self.ip
            vc.devicesName = UIDevice.modelName
            vc.isConnectType = self.connectionType.hashValue
            vc.imei = self.imei
            vc.pointCheck = self.pointCheckSend
            vc.lat = self.lat
            vc.long = self.long
            vc.pointCheckAgain = self.pointCheck
            vc.typeConnect = self.getConnectionType()
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnDropList_tap(_ sender: Any) {
        let vc = ServerVC()
        vc.pointCheck = self.pointCheck
        vc.delegate = self
        vc.id = 1
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func getOSInfo()->String {
        let os = ProcessInfo.processInfo.operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    func getTokenKey(completionHandler: @escaping () -> Void) {
        let headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        
        let paramaters : Parameters = ["grant_type": "client_credentials"]
        
        AF.request("https://api.i-speed.vn/oauth/token", method: .post, parameters: paramaters, encoding: URLEncoding.httpBody, headers: headers ).authenticate(username: "25fe94ac-4fba-4731-8792-ee26e76c5ebb", password:"Uhb9z0pljvqcAZtJ7XGe26nksFmV31CTuYRS").responseData { response in
            debugPrint(response)
            if response.error == nil {
                if let data = response.data {
                    do {
                        let point = try JSONDecoder().decode(TokenModel.self, from: data)
                        self.token = point
                        let defaults = UserDefaults.standard
                        defaults.set(self.token?.access_token, forKey: "TOKEN")
                        completionHandler()
                        
                    } catch {
                        print(error)
                    }
                }
            }
        }
    }
    
    func getNameCity(lat: Double, long: Double) {
        guard let urlPoint = URL(string: "https://api.i-speed.vn/v3/locations?lat=\(lat)&lon=\(long)") else {return}
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        
        AF.request(urlPoint , method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: header).responseJSON { [weak self] (response) in
            if let data = response.data {
                let json = try? JSON(data: response.data!)
                print("JSONJSON\(json)")
                DispatchQueue.main.async {
                    self?.nameLocationCity.text = json?["provinceName"].stringValue
                }
            }
        }
    }
    
    func getIPV4FromServer(completionHandler: @escaping () -> Void) {
        guard let urlPoint = URL(string: "https://api-v4.i-speed.vn/v3/ip-info") else {return}
        let auth = UserDefaults.standard.value(forKey: "TOKEN")
        let headers: HTTPHeaders = [.authorization(bearerToken: auth as! String)]
        AF.request(urlPoint , method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { [weak self] (response) in
            if let data = response.data {
                do {
                    let point = try JSONDecoder().decode(IPModel.self, from: data)
                    self?.ip = point
                    guard let name = self?.ip?.isp else {return}
                    DispatchQueue.main.async {
                        self?.SSIDNetWork.fadeTransition(0.4)
                        self?.SSIDNetWork.text = "\(String(describing: name))"
                        self?.imgLoadingName.isHidden = true
                    }
                    if point.ip != "" {
                        self?.ipV4 = point.ip ?? ""
                        UserDefaults.standard.setValue(point.ip, forKey: "externalAddress")
                        DispatchQueue.main.async {
                            self?.StatusIPv4.textColor = .white
                            self?.btnIPv4.isEnabled = true
                        }
                        completionHandler()
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func getIPV6FromServer() {
        self.ipV6.removeAll()
        guard let urlPoint = URL(string: "https://api-v6.i-speed.vn/v3/ip-info") else {return}
        let auth = UserDefaults.standard.value(forKey: "TOKEN")
        let headers: HTTPHeaders = [.authorization(bearerToken: auth as! String)]
        AF.request(urlPoint , method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { [weak self] (response) in
            guard let sSelf = self else {return}
            if let data = response.data {
                do {
                    let point = try JSONDecoder().decode(IPModel.self, from: data)
                    if sSelf.ipV6.contains(where: {$0.ip == point.ip}) {
                    } else {
                        sSelf.ipV6.append(point)
                    }
                    if point.ip != "" {
                        DispatchQueue.main.async {
                            sSelf.statusIPv6.textColor = .white
                            sSelf.btnIPv6.isEnabled = true
                        }
                        
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func getAllServer() {
        self.pointCheck?.removeAll()
        self.newDataArr.removeAll()
        self.newDataSet.removeAll()
        let text: String = "\(self.lat ?? ""),\(self.long ?? "")"
        var param: Parameters = [:]
        guard let urlPoint = URL(string: "https://api.i-speed.vn/v3/servers") else {return}
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        print(token as Any)
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        if self.lat == nil && self.long == nil {
            param = [:]
        } else {
            param = [
                "location": text
            ]
        }
        AF.sessionConfiguration.timeoutIntervalForRequest = 10
        AF.sessionConfiguration.timeoutIntervalForResource = 10
        AF.request(urlPoint , method: .get, parameters: param, encoding: URLEncoding.default, headers: header).responseData { [weak self] (response) in
            switch response.result {
            case .success(_):
                if let data = response.data {
                    do {
                        let point = try JSONDecoder().decode([PointCheckModel].self, from: data)
                        self?.pointCheck = point
                        self?.startTestPingHome(attempts: 1, pointCheck: point)
                        
                    } catch {
                        print(error)
                    }
                }
            case .failure(let error):
                switch error {
                case .sessionTaskFailed(URLError.timedOut):
                    let date = Date()
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let dateString = df.string(from: date)
                    UserDefaults.standard.set(dateString, forKey: "errorTime")
                    let alert = UIAlertController(title: "Chú ý", message: "Không kết nối được đến máy chủ, xin vui lòng kiểm tra lại kết nối mạng của bạn và thử lại!", preferredStyle: UIAlertController.Style.alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .cancel)
                    alert.addAction(cancelAction)
                    self?.present(alert, animated: true, completion: nil)
                default:
                    let date = Date()
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let dateString = df.string(from: date)
                    UserDefaults.standard.set(dateString, forKey: "errorTime")
                    let alert = UIAlertController(title: "Chú ý", message: "Không lấy được thông tin, vui lòng thử lại sau", preferredStyle: UIAlertController.Style.alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .cancel)
                    alert.addAction(cancelAction)
                    self?.present(alert, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func removeObject() {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "errorTime")
        prefs.removeObject(forKey: "clientCode")
        prefs.removeObject(forKey: "networkType")
        prefs.removeObject(forKey: "externalAddress")
        prefs.removeObject(forKey: "address")
        prefs.removeObject(forKey: "latitude")
        prefs.removeObject(forKey: "longitude")
    }
    
    func convertLocationToCity(lat: CLLocationDegrees, long: CLLocationDegrees) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler:
                                            {
            placemarks, error -> Void in
            
            // Place details
            guard let placeMark = placemarks?.first else { return }
            // City
            if let city = placeMark.subAdministrativeArea {
                print(city)
                UserDefaults.standard.set(city, forKey: "address")
                
            }
        })
        
    }
    
    func sendError() {
        let errorTime = UserDefaults.standard.value(forKey: "errorTime")
        let clientCode = UserDefaults.standard.value(forKey: "clientCode")
        let networkType = UserDefaults.standard.value(forKey: "networkType")
        let externalAddress = UserDefaults.standard.value(forKey: "externalAddress")
        let address = UserDefaults.standard.value(forKey: "address")
        let latitude = UserDefaults.standard.value(forKey: "latitude")
        let longitude = UserDefaults.standard.value(forKey: "longitude")
        let urlPoint = "https://api.i-speed.vn/v3/info-errors"
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        print(token as Any)
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        let param: Parameters = [
            "errorTime": errorTime as Any,
            "clientCode": clientCode as Any,
            "networkType": networkType as Any,
            "externalAddress": externalAddress as Any,
            "address": address as Any,
            "latitude": latitude as Any,
            "longitude": longitude as Any,
            
        ]
        print("param: \(param)")
        AF.request(urlPoint , method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseData { [weak self] (response) in
            switch response.result {
            case .success(_):
                let json = try? JSON(data: response.data!)
                self?.removeObject()
                print("jsonERROR:\(json ?? "")")
            case .failure(_): break
                
            }
        }
    }
    
    
    func startTestPingHome(attempts: Int? , pointCheck: [PointCheckModel]?) {
        // Reset the values.
        self.ping = 0
        self.jitter = 0
        
        self.attemptPings = attempts ?? Constants.Ping.attempts
        attemptPing = 1
        guard let count = pointCheck?.count else {
            return
        }
        if (count > 10) {
            for index in 0..<10 {
                self.pointCheck?[index].pingValue = index
                if let model = self.pointCheck?[index] {
                    pingServerHome(dataPoint: model, indexPoint: index)
                }
            }
        }
    }
    
    
    
    private func pingServerHome(dataPoint: PointCheckModel , indexPoint: Int) {
        var pingString: String!
        pingString = (dataPoint.baseUrl ?? "") + "/" + (dataPoint.pingUrl ?? "")
        guard let pingURL = URL(string: pingString) else { return }
        
        var request = URLRequest(url: pingURL, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 2)
        
        let urlconfig = URLSessionConfiguration.default
        urlconfig.timeoutIntervalForRequest = 2
        urlconfig.timeoutIntervalForResource = 2
        
        request.httpMethod = "HEAD"
        let session = URLSession(configuration: urlconfig, delegate: self, delegateQueue: nil)//URLSession.shared
        
        request.timeoutInterval = 2
        session.dataTask(with: request) { [unowned self] data, response, error in
            
            if let e = error {
                // khong tra ve gi thi remove
                print(">>>>>> [URLDataTask completion]: \(e.localizedDescription)")
                return
            } else {
                // tra ve gia tri
                if !self.newDataArr.contains(obj: dataPoint) {
                    self.newDataArr.append(dataPoint)
                }
                print("newDataArr: \(self.newDataArr)")
                self.startPingHome = Date.timeIntervalSinceReferenceDate
                self.updateValuesHome(isError: false)
            }
        }.resume()
        
    }
    
    private func updateValuesHome(isError: Bool) {
        let timeElapsed = Date.timeIntervalSinceReferenceDate - self.startPingHome
        let ping = self.ping
        let jitter = self.jitter
        
        let newJitter: Double
        if ping == 0.0 {
            newJitter = 0.0
        } else {
            let difference = abs((ping - timeElapsed) / 10.0)
            let priority: (old: Double, new: Double) = difference > jitter ? (0.3, 0.7) : (0.8, 0.2)
            newJitter = jitter * priority.old + difference * priority.new
        }
        self.ping = timeElapsed
        self.jitter = newJitter
        print("Ping: \(self.ping)")
    }
    
    
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                if #available(iOS 13.0, *) {
                    var name: String!
                    let dataServiceIdentifier = networkInfo.dataServiceIdentifier
                    for (service, _) in networkInfo.serviceSubscriberCellularProviders ?? [:] {
                        if (dataServiceIdentifier == service) {
                            let radio = networkInfo.serviceCurrentRadioAccessTechnology?[service] ?? ""
                            if #available(iOS 14.1, *) {
                                switch radio {
                                case CTRadioAccessTechnologyNR, CTRadioAccessTechnologyNRNSA:
                                    name = "5G"
                                    return name
                                case CTRadioAccessTechnologyGPRS,
                                    CTRadioAccessTechnologyEdge,
                                CTRadioAccessTechnologyCDMA1x:
                                    name = "2G"
                                    return name
                                    
                                case CTRadioAccessTechnologyLTE:
                                    name = "LTE"
                                    return name
                                case CTRadioAccessTechnologyWCDMA,
                                    CTRadioAccessTechnologyHSDPA,
                                    CTRadioAccessTechnologyHSUPA,
                                    CTRadioAccessTechnologyCDMAEVDORev0,
                                    CTRadioAccessTechnologyCDMAEVDORevA,
                                    CTRadioAccessTechnologyCDMAEVDORevB,
                                CTRadioAccessTechnologyeHRPD:
                                    name = "3G"
                                    return name
                                    
                                    
                                default:
                                    name = "Unknown"
                                    return name
                                }
                            } else {
                                // Fallback on earlier versions
                                switch radio {
                                case CTRadioAccessTechnologyGPRS,
                                    CTRadioAccessTechnologyEdge,
                                CTRadioAccessTechnologyCDMA1x:
                                    name = "2G"
                                    return name
                                case CTRadioAccessTechnologyLTE:
                                    name = "LTE"
                                    return name
                                case CTRadioAccessTechnologyWCDMA,
                                    CTRadioAccessTechnologyHSDPA,
                                    CTRadioAccessTechnologyHSUPA,
                                    CTRadioAccessTechnologyCDMAEVDORev0,
                                    CTRadioAccessTechnologyCDMAEVDORevA,
                                    CTRadioAccessTechnologyCDMAEVDORevB,
                                CTRadioAccessTechnologyeHRPD:
                                    name = "3G"
                                    return name
                                    
                                default:
                                    name = "Unknown"
                                    return name
                                }
                            }
                            
                        }
                    }
                    return name
                } else {
                    // Fallback on earlier versions
                    let networkInfo = CTTelephonyNetworkInfo()
                    let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                    
                    guard let carrierTypeName = carrierType?.first?.value else {
                        return "UNKNOWN"
                    }
                    
                    if #available(iOS 14.1, *) {
                        switch carrierTypeName {
                        case CTRadioAccessTechnologyNR, CTRadioAccessTechnologyNRNSA:
                            return "5G"
                        case CTRadioAccessTechnologyGPRS,
                            CTRadioAccessTechnologyEdge,
                        CTRadioAccessTechnologyCDMA1x:
                            return "2G"
                        case CTRadioAccessTechnologyLTE:
                            return "LTE"
                        case CTRadioAccessTechnologyWCDMA,
                            CTRadioAccessTechnologyHSDPA,
                            CTRadioAccessTechnologyHSUPA,
                            CTRadioAccessTechnologyCDMAEVDORev0,
                            CTRadioAccessTechnologyCDMAEVDORevA,
                            CTRadioAccessTechnologyCDMAEVDORevB,
                        CTRadioAccessTechnologyeHRPD:
                            return "3G"
                            
                        default:
                            return "Unknown"
                        }
                    } else {
                        // Fallback on earlier versions
                        switch carrierTypeName {
                        case CTRadioAccessTechnologyGPRS,
                            CTRadioAccessTechnologyEdge,
                        CTRadioAccessTechnologyCDMA1x:
                            return "2G"
                        case CTRadioAccessTechnologyLTE:
                            return "LTE"
                        case CTRadioAccessTechnologyWCDMA,
                            CTRadioAccessTechnologyHSDPA,
                            CTRadioAccessTechnologyHSUPA,
                            CTRadioAccessTechnologyCDMAEVDORev0,
                            CTRadioAccessTechnologyCDMAEVDORevA,
                            CTRadioAccessTechnologyCDMAEVDORevB,
                        CTRadioAccessTechnologyeHRPD:
                            return "3G"
                            
                        default:
                            return "Unknown"
                        }
                    }
                }
            }else {
                return "WIFI"
            }
            
        } else {
            return "NO INTERNET"
        }
    }
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
#if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPhone13,1":                              return "iPhone 12 mini"
            case "iPhone13,2":                              return "iPhone 12"
            case "iPhone13,3":                              return "iPhone 12 Pro"
            case "iPhone13,4":                              return "iPhone 12 Pro Max"
            case "iPhone14,4":                              return "iPhone 13 mini"
            case "iPhone14,5":                              return "iPhone 13"
            case "iPhone14,2":                              return "iPhone 13 Pro"
            case "iPhone14,3":                              return "iPhone 13 Pro Max"
            case "iPhone14,7":                              return "iPhone 14"
            case "iPhone14,8":                              return "iPhone 14 Plus"
            case "iPhone15,2":                              return "iPhone 14 Pro"
            case "iPhone15,3":                              return "iPhone 14 Pro Max"
            case "iPhone15,4":                              return "iPhone 15"
            case "iPhone15,5":                              return "iPhone 15 Plus"
            case "iPhone16,1":                              return "iPhone 15 Pro"
            case "iPhone16,2":                              return "iPhone 15 Pro Max"
            case "iPhone14,6":                              return "iPhone SE (3rd generation)"
                
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "AudioAccessory5,1":                       return "HomePod mini"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
#elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
#endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}
extension UIViewController {
    func addChildren(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
extension UIView {
    func pushTransition() {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
                                                            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.subtype = CATransitionSubtype.fromTop
        animation.duration = 0.03
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}



