//
//  AppDelegate.swift
//  I-Speed
//
//  Created by Le Hung on 3/3/21.
//

import UIKit
import CoreData
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window:UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure() // gọi hàm để cấu hình 1 app Firebase mặc định
        Messaging.messaging().delegate = self //Nhận các message từ FirebaseMessaging
        configApplePush(application) // đăng ký nhận push.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = SplashVC()
        window?.makeKeyAndVisible()
        UITabBar.appearance().layer.borderWidth = 0.5
        UITabBar.appearance().layer.borderColor = UIColor.black.cgColor
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        UITabBar.appearance().backgroundImage = UIImage.from(color: .clear)
        UITabBar.appearance().barTintColor = #colorLiteral(red: 0.1176470588, green: 0.007843137255, blue: 0.1843137255, alpha: 1)
        UITabBar.appearance().tintColor = #colorLiteral(red: 0.937254902, green: 0.3647058824, blue: 0.6588235294, alpha: 1)
        UITabBar.appearance().backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.007843137255, blue: 0.1843137255, alpha: 1)
        UITabBar.appearance().isTranslucent = false
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            let vc1 = HomeViewController()
            vc1.tabBarItem.image = UIImage(named: "speed")
            vc1.tabBarItem.imageInsets = UIEdgeInsets(top: 15, left: 0, bottom: -5, right: 0)
            
            let vc2 = LocalTestResultVC()
            vc2.tabBarItem.image = UIImage(named: "Group")
            vc2.tabBarItem.imageInsets = UIEdgeInsets(top: 15, left: 0, bottom: -5, right: 0)
            
            let vc3 = MoreVC()
            vc3.tabBarItem.image = UIImage(named: "icons8-settings-72")
            vc3.tabBarItem.imageInsets = UIEdgeInsets(top: 15, left: 0, bottom: -5, right: 0)
            let vc4 = NotifiVC()
                vc4.tabBarItem.image = UIImage(named: "icons8-notification-72")
            
            vc4.tabBarItem.imageInsets = UIEdgeInsets(top: 15, left: 0, bottom: -5, right: 0)
            
            let tabBarController = UITabBarController()
            tabBarController.viewControllers = [vc1, vc2, vc3, vc4]
            
            let navController = UINavigationController(rootViewController: tabBarController)
            self.window?.rootViewController = navController
            
        }
        
        //FirebaseApp.configure()
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TestResultModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func configApplePush(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        if let token = Messaging.messaging().fcmToken {
            print("FCM token: \(token)")
            //AppSession.shared.setFirebaseToken(token)
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

