//
//  SpeedTestVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/6/21.
//

import UIKit
import Combine
import Foundation
import MBCircularProgressBar
import Alamofire
import SwiftyJSON
import CoreData
import Cosmos
import Network
import DropDown
import CoreTelephony
import FGRoute
import SystemConfiguration.CaptiveNetwork


struct WiFiInfo {
    var rssi: String
    var networkName: String
    var macAddress: String
}

struct Banner {
    var name: String?
}

enum AppKeys: String {
    case foregroundView         = "foregroundView"
    case numberOfActiveBars     = "numberOfActiveBars"
    case statusBar              = "statusBar"
    case wifiStrengthRaw        = "wifiStrengthRaw"
}

enum WiFISignalStrength: Int {
    case weak = 0
    case ok = 1
    case veryGood = 2
    case excellent = 3
    
    func convertBarsToDBM() -> Int {
        switch self {
        case .weak:
            return -90
        case .ok:
            return -70
        case .veryGood:
            return -50
        case .excellent:
            return -30
        }
    }
}




class SpeedTestVC: UIViewController, SendDataDelegateToTest, UITextFieldDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    func sendData(pointCheck: PointCheckModel) {
        self.pointCheck = pointCheck
        self.pingArr?.removeAll()
        self.jitterArr?.removeAll()
        self.reset()
        self.valueJitter.text = ""
        self.valuePing.text = ""
        self.StartTest()
        self.progressLineDown.progress = 0.0
        self.progressLineUp.progress = 0.0
        
    }
    var isConnectType: Int?
    let numberOfThreads = 10 // Number of concurrent download threads
    var totalBytesDownloaded: Int64 = 0
    var startTime: DispatchTime?
    
    private var downloadTasks: [URLSessionDownloadTask] = []
    private var totalBytesDownloadedMulti: Int64 = 0
    private var startTimeMulti: DispatchTime?
    
    private let byteFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = [.useKB, .useMB]
        return formatter
    }()
    
    
    let info: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
    
    var lat: String?
    var long: String?
    var pointCheck: PointCheckModel?
    var ipAdress: IPModel?
    var devicesName: String?
    var imei: String?
    var typeConnect: String?
    var testResult: [NSManagedObject] = []
    var isFinish: Bool = false
    var down = 0.0
    var up = 0.0
    let dropDown = DropDown()
    var ratingInt: Int?
    var commentText: String?
    var wifiInfo: WiFiInfo?
    
    var timer:Timer!
    var count = 0
    var download = 0.0
    var upload = 0.0
    var pingArr: [Double]? = []
    var jitterArr: [Double]? = []
    var PingS: Double?
    var JitterS: Double?
    let group = DispatchGroup()
    var ping = 0.0 {
        didSet {
            print("ping\(self.ping)")
            if self.ping > 0.0 {
                pingArr?.append(ping)
                guard let valuePing = pingArr?.reduce(.infinity, {min($0, $1)}) else {return}
                print("value: \(valuePing)")
                let result: (value: String, unit: String)?
                if valuePing > 1 {
                    result = Formatters.secondFormatter.string(for: valuePing).map {( $0, "s")}
                } else {
                    let miniseconds = valuePing * 1000
                    result = Formatters.millisecondFormatter.string(for: miniseconds).map{($0, "ms")}
                }
                if let result = result {
                    DispatchQueue.main.async {
                        self.PingS = Double(result.value)
                        self.valuePing.text = result.value + result.unit
                    }
                }
            }
        }
    }
    var jitter = 0.0 {
        didSet {
            print("jitter\(jitter)")
            if self.jitter > 0.0 {
                let result: (value: String, unit: String)?
                if jitter > 1 {
                    result = Formatters.secondFormatter.string(for: jitter).map {( $0, "s")}
                } else {
                    let miniseconds = jitter * 1000
                    result = Formatters.millisecondFormatterJitter.string(for: miniseconds).map{($0, "ms")}
                }
                if let result = result {
                    DispatchQueue.main.async {
                        self.valueJitter.text = result.value + result.unit
                    }
                }
            }
        }
    }
    var urlShare: URL?
    var url: String? {
        didSet {
            self.urlShare = URL(string: url ?? "")
        }
    }
    
    
    @IBOutlet weak var viewCornerBottom: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viewUpload: UIView!
    @IBOutlet weak var viewDownload: UIView!
    @IBOutlet weak var progressLineDown: UIProgressView!
    @IBOutlet weak var progressLineUp: UIProgressView!
    @IBOutlet weak var iconNetwork: UIImageView!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet weak var nameLocationTest: UILabel!
    @IBOutlet weak var valuePing: UILabel!
    @IBOutlet weak var valueJitter: UILabel!
    @IBOutlet weak var progressUpload: MBCircularProgressBarView!
    @IBOutlet weak var progressDownload: MBCircularProgressBarView!
    @IBOutlet weak var btnTestAgain: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var nameCity: UILabel!
    
    @IBOutlet weak var btnShareResult: UIButton!
    @IBOutlet weak var ipV: UILabel!
    @IBOutlet weak var ispName: UILabel!
    @IBOutlet weak var dtnShowLocation: UIButton!
    @IBOutlet weak var nameConnect: UILabel!
    @IBOutlet weak var commentTextfield: UITextField!
    @IBOutlet weak var lbSpeedInter: UILabel!
    @IBOutlet weak var imageSpeedGuide: UIImageView!
    @IBOutlet weak var textGuide: UILabel!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var heightContent: NSLayoutConstraint!
    @IBOutlet var pageCollectionView: UICollectionView!
    
    @IBOutlet weak var StackThamKhao: UIStackView!
    
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgWeb: UIImageView!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var imgGame: UIImageView!
    
    
    
    @IBOutlet weak var progressEmail: GradientProgressView!
    
    @IBOutlet weak var progressWeb: GradientProgressView!
    @IBOutlet weak var progressShare: GradientProgressView!
    
    @IBOutlet weak var progressGaming: GradientProgressView!
    @IBOutlet weak var progressVideo: GradientProgressView!
    @IBOutlet weak var progressCall: GradientProgressView!
    @IBOutlet weak var viewTextfield: UIView!
    var monitor = NWPathMonitor()
    let queuemonitor = DispatchQueue(label: "NetworkMonitoring")
    private var attemptPings = 0
    private var attemptPing = 1
    private var startPing: TimeInterval!
    private var sessionPing: URLSession!
    private var taskPing: URLSessionDataTask!
    private let queuePing = OperationQueue()
    
    private var dummyData: Data!
    
    private var startDownload: TimeInterval!
    private var attemptDownload = 1
    private var sessionDownload: URLSession!
    private var taskDownload: URLSessionDownloadTask!
    private let queueDownload = OperationQueue()
    
    private var startUpload: TimeInterval!
    private var attemptUpload = 1
    private var sessionUpload: URLSession!
    private var taskUpload: URLSessionUploadTask!
    private let queueUpload = OperationQueue()
    var netWorkNow: String!
    var netWorkBefore: String!
    var pointCheckAgain: [PointCheckModel]?
    
    
    var connectionTypeFirst: ConnectionType = .unknown

    enum ConnectionType {
        case wifi
        case cellular
        case ethernet
        case unknown
    }
    
    let cellWidth = (3 / 4) * UIScreen.main.bounds.width
    let sectionSpacing = (1 / 8) * UIScreen.main.bounds.width
    let cellSpacing = (1 / 16) * UIScreen.main.bounds.width
    
    var colors: [UIImage?] = [UIImage(named: "1Mb"), UIImage(named: "5Mb"), UIImage(named: "40Mb"), UIImage(named: "100Mb"), UIImage(named: "500Mb")]
    let cellId = "ContentCell"
    //MARK:INdex
    var index: Int? = nil {
        didSet {
            self.pageCollectionView.reloadData()
            self.pageCollectionView.scrollToItem(at: IndexPath(row: index ?? 1, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: .connectionTypeChanged, object: nil)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCornerBottom.clipsToBounds = true
        viewCornerBottom.layer.cornerRadius = 30
        viewCornerBottom.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        StackThamKhao.isHidden = true
        scrollView.isScrollEnabled = false
        let contentRect: CGRect = scrollView.subviews.reduce(into: .zero) { partialResult, view in
            partialResult = partialResult.union(view.frame)
        }
        scrollView.contentSize = contentRect.size
        //heightContent.constant = 850
        self.pageControl.numberOfPages = colors.count
        //Set the delegate
        //self.pageCollectionView.delegate = self
        dismissKeyboard()
        dtnShowLocation.layer.cornerRadius = 14
        dtnShowLocation.clipsToBounds = true
        dtnShowLocation.layer.borderWidth = 1
        dtnShowLocation.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        btnShare.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        btnShare.layer.cornerRadius = 16
        btnShare.clipsToBounds = true
        btnShare.layer.borderWidth = 1
        btnTestAgain.layer.borderColor = #colorLiteral(red: 0.937254902, green: 0.3647058824, blue: 0.6588235294, alpha: 1)
        btnTestAgain.layer.cornerRadius = 16
        btnTestAgain.clipsToBounds = true
        btnTestAgain.layer.borderWidth = 1
        btnShareResult.layer.cornerRadius = 16
        btnShareResult.clipsToBounds = true
        btnShareResult.layer.borderWidth = 1
        btnShareResult.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        viewDownload.layer.cornerRadius = 20
        viewDownload.clipsToBounds = true
        viewDownload.layer.borderWidth = 1
        viewDownload.layer.borderColor = #colorLiteral(red: 0.6941176471, green: 0.6941176471, blue: 0.6901960784, alpha: 1)
        viewUpload.layer.cornerRadius = 20
        viewUpload.clipsToBounds = true
        viewUpload.layer.borderWidth = 1
        viewUpload.layer.borderColor = #colorLiteral(red: 0.6941176471, green: 0.6941176471, blue: 0.6901960784, alpha: 1)
        viewTextfield.layer.cornerRadius = 20
        viewTextfield.clipsToBounds = true
        viewTextfield.layer.borderWidth = 1
        viewTextfield.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        commentTextfield.attributedPlaceholder =
        NSAttributedString(string: "Nhập bình luận của bạn", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6941176471, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
        //commentTextfield.sizeToFit()
        //commentTextfield.textAlignment = .center
        nameConnect.text = devicesName
        self.dtnShowLocation.isHidden = true
        if let _ = IPv4Address(self.ipAdress?.ip ?? "") {
            //IP v4
            //            self.ipV.text =  "(IPv4)"
        } else if let _ = IPv6Address(self.ipAdress?.ip ?? "") {
            //            self.ipV.text = "(IPv6)"
            // IP v6
        }
        self.ispName.text = self.ipAdress?.isp
        let text = self.typeConnect
        if text == "LTE" {
            self.iconNetwork.image = UIImage(named: "LTE")
        }else if text == "3G" {
            self.iconNetwork.image = UIImage(named: "3G")
            
        }else if text == "WIFI" {
            self.iconNetwork.image = UIImage(named: "icon-wifi-2")
            
        }else if text == "5G" {
            self.iconNetwork.image = UIImage(named: "5G")
            
        }else if text == "2G" {
            self.iconNetwork.image = UIImage(named: "2G")
            
        }
        if typeConnect == "WIFI" {
            //self.wifiInfo = self.getWiFiInfo()
        }
        progressDownload.progressColor = #colorLiteral(red: 0.4588235294, green: 0.8862745098, blue: 1, alpha: 1)
        progressUpload.progressColor = #colorLiteral(red: 0.9960784314, green: 0.3843137255, blue: 0.3843137255, alpha: 1)
        ratingView.settings.fillMode = .full
        NotificationCenter.default.addObserver(self, selector: #selector(pauseSong) , name:UIApplication.didEnterBackgroundNotification, object: nil)
        let layout = PagingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = pageCollectionView.frame.size
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        pageCollectionView.setCollectionViewLayout(layout, animated: false)
        pageCollectionView.isPagingEnabled = false
        pageCollectionView.alwaysBounceVertical = false
        pageCollectionView.dataSource = self
        pageControl.isHidden = true
        pageCollectionView.isHidden = true
        viewTextfield.isHidden = true
        //heightCollectionView.constant = 0
        design()
        registerCollectionViewCells()
        registerForKeyboardNotifications()
        NetworkMonitoring.shared.onStatusChange = { [weak self] isConnected, interfaceType, isExpensive in
            self?.handleNetworkChange(isConnected: isConnected, interfaceType: interfaceType, isExpensive: isExpensive)
        }
//        NetworkMonitor.shared.startMonitoring()
        // Observe network changes
//        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged), name: .connectionTypeChanged, object: nil)
        //startTestPing(attempts: 10)
        StartTest()
        
        
    }
    
    func handleNetworkChange(isConnected: Bool, interfaceType: NWInterface.InterfaceType, isExpensive: Bool) {
        // Xử lý thay đổi mạng ở đây
        if isConnected {
            print("Màn hình 2: Đã kết nối qua \(interfaceType)")
            print("Màn hình 2: Đã kết nối qua \(isExpensive)")
            if isExpensive == false {
                DispatchQueue.main.async {
                    self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
                }
                
            }
        } else {
            print("Màn hình 2: Mất kết nối")
            DispatchQueue.main.async {
                self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
            }
        }
    }
    
    @objc func networkStatusChanged(notification: Notification) {
        let monitor = NetworkMonitor.shared
        print("Network status changed: \(monitor.connectionType)")
        if isConnectType != monitor.connectionType.hashValue {
            DispatchQueue.main.async {
                self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
            }
            
        }
        
        
        // Update UI or handle changes here
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //    func checkDownloadSpeed(url: URL, numberOfThreads: Int ) {
    //        totalBytesDownloaded = 0
    //        startTime = DispatchTime.now()
    //
    //        let group = DispatchGroup()
    //        let chunkSize = 100 * 1024 * 1024 // 100 MB chunks
    //
    //        for _ in 0..<numberOfThreads {
    //            group.enter()
    //            let downloadTask = createDownloadTask(url: url, chunkSize: chunkSize)
    //            downloadTask.resume()
    //            downloadTasks.append(downloadTask)
    //            group.leave()
    //        }
    //
    //        group.notify(queue: .main) {
    //            let endTime = DispatchTime.now()
    //            let elapsedTime = Double(endTime.uptimeNanoseconds - self.startTime!.uptimeNanoseconds) / 1_000_000_000.0
    //            let downloadSpeed = Double(self.totalBytesDownloaded) / (1024 * 1024) / elapsedTime // MB/s
    //            print("downloadSpeed\(downloadSpeed)")
    //            DispatchQueue.main.async {
    //                self.progressLineDown.progress = Float(downloadSpeed) / Float(100*1048576)
    //                self.download = downloadSpeed
    //                self.down = downloadSpeed
    //                if downloadSpeed < 1000 {
    //                    if(downloadSpeed > 100) {
    //                        self.progressDownload.maxValue = CGFloat(downloadSpeed)
    //                    } else {
    //                        self.progressDownload.maxValue = 100.0
    //
    //                    }
    //                    self.progressDownload.progressAngle = 80
    //                    self.progressDownload.value = CGFloat(downloadSpeed)
    //                }
    //            }
    //            //completion(downloadSpeed)
    //
    //        }
    //        group.wait()
    //        print("Download Completed")
    //    }
    
    func createDownloadTask(url: URL, chunkSize: Int) -> URLSessionDownloadTask {
        var request = URLRequest(url: url)
        request.addValue("bytes=\(totalBytesDownloaded)-", forHTTPHeaderField: "Range")
        
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        return session.downloadTask(with: request)
    }
    //}
    
    
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 300, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.commentTextfield {
            if (!aRect.contains(commentTextfield.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height + 300, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        commentTextfield = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        commentTextfield = nil
    }
    
    
    
    
    private func design() {
        view.backgroundColor = .white
    }
    
    private func registerCollectionViewCells() {
        
        let nib = UINib(nibName: "CustomCell", bundle: nil)
        self.pageCollectionView.register(nib, forCellWithReuseIdentifier: "CustomCell")
    }
    
    
    func getWiFiInfo() -> WiFiInfo? {
        guard let interfaces = CNCopySupportedInterfaces() as? [String] else {
            return nil
        }
        var wifiInfo: WiFiInfo? = nil
        for interface in interfaces {
            guard let interfaceInfo = CNCopyCurrentNetworkInfo(interface as CFString) as NSDictionary? else {
                return nil
            }
            guard let ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String else {
                return nil
            }
            guard let bssid = interfaceInfo[kCNNetworkInfoKeyBSSID as String] as? String else {
                return nil
            }
            var rssi: Int = 0
            if let strength = getWifiStrength() {
                rssi = strength
            }
            wifiInfo = WiFiInfo(rssi: "\(rssi)", networkName: ssid, macAddress: bssid)
            break
        }
        return wifiInfo
    }
    
    private func getWifiStrength() -> Int? {
        return getWifiStrengthOnDevicesExceptIphoneX()
    }
    
    private func getWifiStrengthOnDevicesExceptIphoneX() -> Int? {
        var rssi: Int?
        guard let statusBar = UIApplication.shared.value(forKey:"statusBar") as? UIView,
              let foregroundView = statusBar.value(forKey:"foregroundView") as? UIView else {
            return rssi
        }
        for view in foregroundView.subviews {
            if let statusBarDataNetworkItemView = NSClassFromString("UIStatusBarDataNetworkItemView"),
               view.isKind(of: statusBarDataNetworkItemView) {
                if let val = view.value(forKey:"wifiStrengthRaw") as? Int {
                    rssi = val
                    break
                }
            }
        }
        return rssi
    }
    
    
    
    @IBAction func send_comment(_ sender: Any) {
        if (ratingInt != nil) {
            updateRating(rating: ratingInt!, comment: commentTextfield.text ?? "")
        } else {
            self.showAlertWarning(title: "", content: "Xin vui lòng đánh giá chất lượng kết nối trước khi gửi bình luận")
            
        }
    }
    
    func receiveInfoNetwork() {
        let before = (UserDefaults.standard.string(forKey: "NetWork") ?? "") as String
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                if path.usesInterfaceType(.wifi) {
                    if before != "wifi" {
                        DispatchQueue.main.async {
                            self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
                        }
                        UserDefaults.standard.setValue("wifi", forKey: "NetWork")
                    } else {
                        DispatchQueue.main.async {
                            self.StartTest()
                        }
                        
                    }
                    
                } else if path.usesInterfaceType(.cellular) {
                    if before != "cellular" {
                        DispatchQueue.main.async {
                            self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
                        }
                        UserDefaults.standard.setValue("cellular", forKey: "NetWork")
                    } else {
                        DispatchQueue.main.async {
                            self.StartTest()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(title: "Kết nối Internet thay đổi", content: "Kết nối Internet của bạn vừa bị thay đổi, xin vui lòng quay lại màn hình chính để thực hiện đo tốc độ kết nối!")
                }
            }
        }
        monitor.start(queue: queuemonitor)
    }
    
    func showAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) {[weak self] (_) -> Void in
            self?.reset()
            //let dic = ["myMessage": "changeNetwork"]
            //NotificationCenter.default.post(name: NSNotification.Name.init("ChangeNetwork"), object: nil, userInfo: dic)
            DispatchQueue.main.async {
                self?.navigationController?.popViewController(animated: true)
            }
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWarning(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel) {[weak self] (_) -> Void in
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    let name: String = String(cString: (interface!.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
    
    func StartTest() {
        self.scrollView.setContentOffset(.zero, animated: true)
        self.scrollView.isScrollEnabled = false
        self.btnShareResult.isEnabled = true
        self.commentTextfield.isEnabled = true
        self.commentTextfield.text?.removeAll()
        self.commentTextfield.attributedPlaceholder =
        NSAttributedString(string: "Nhập bình luận của bạn", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6941176471, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
        self.ratingView.settings.updateOnTouch = true
        self.progressDownload.value = 0.0
        self.progressUpload.value = 0.0
        self.progressLineDown.progress = 0.0
        self.progressLineUp.progress = 0.0
        self.ratingView.rating = 0
        self.startTestPing(attempts: 10)
        //self.newTestPintStart(attempts: 10)
        self.nameLocationTest.text = self.pointCheck?.name ?? ""
        self.nameCity.text = self.pointCheck?.city ?? ""
        self.btnTestAgain.isHidden = true
        self.btnShare.isHidden = true
        self.ratingView.isHidden = true
        self.textGuide.isHidden = true
        self.dtnShowLocation.isHidden = true
        self.commentTextfield.isHidden = true
        self.btnShareResult.isHidden = true
        self.lbSpeedInter.isHidden = true
        self.viewTextfield.isHidden = true
        //self.imageSpeedGuide.isHidden = true
        self.pageCollectionView.isHidden = true
        //self.heightCollectionView.constant = 0
        self.pageControl.isHidden = true
        self.ratingView.didFinishTouchingCosmos = { [weak self] rating in
            print("RATING")
            //self?.updateRating(rating: Int(rating))
            self?.ratingInt = Int(rating)
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //monitor.cancel()
        self.pingArr?.removeAll()
        self.jitterArr?.removeAll()
        self.sessionDownload?.invalidateAndCancel()
        self.sessionUpload?.invalidateAndCancel()
        taskPing.cancel()
        taskDownload?.cancel()
        taskUpload?.cancel()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
        self.tabBarController?.tabBar.isHidden = false
        self.reset()
    }
    
    
    @IBAction func showDropDown(_ sender: Any) {
        let vc = ServerVC()
        vc.pointCheck = pointCheckAgain
        vc.delegateTest = self
        vc.id = 2
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @objc func pauseSong(notification : NSNotification) {
        self.reset()
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAllServer() {
        let text: String = "\(self.lat ?? ""),\(self.long ?? "")"
        var param: Parameters = [:]
        guard let urlPoint = URL(string: "https://api.i-speed.vn/v3/servers") else {return}
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        print(token as Any)
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        if self.lat == nil && self.long == nil {
            param = [:]
        } else {
            param = [
                "location": text
            ]
        }
        
        AF.request(urlPoint , method: .get, parameters: param, encoding: URLEncoding.queryString, headers: header).responseJSON { [weak self] (response) in
            if let data = response.data {
                //print(data)
                do {
                    let point = try JSONDecoder().decode([PointCheckModel].self, from: data)
                    self?.pointCheckAgain = point
                } catch {
                    print(error)
                }
            }
        }
    }
    
    
    func updateRating(rating: Int, comment: String) {
        guard let key = UserDefaults.standard.value(forKey: "RESULT") else {return}
        let urlPoint = "https://api.i-speed.vn/v3/results/\(key)"
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        print(token as Any)
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        let param: Parameters = [
            "rating": rating,
            "comment": comment
        ]
        print("param: \(param)")
        AF.request(urlPoint , method: .put, parameters: param, encoding: JSONEncoding.default, headers: header).responseJSON { [weak self] (response) in
            switch response.result {
            case .success(_):
                let json = try? JSON(data: response.data!)
                print(json as Any)
                self?.btnShareResult.isEnabled = false
                self?.commentTextfield.isEnabled = false
                self?.ratingView.settings.updateOnTouch = false
                let alert = UIAlertController(title: "Thành công", message: "Gửi đánh giá chất lượng kết nối Internet thành công!", preferredStyle: UIAlertController.Style.alert)
                let cancelAction = UIAlertAction(title: "OK", style: .cancel)
                alert.addAction(cancelAction)
                self?.present(alert, animated: true, completion: nil)
            case .failure(_):
                let alert = UIAlertController(title: "Có lỗi", message: "Vui lòng thử lại sau", preferredStyle: UIAlertController.Style.alert)
                let cancelAction = UIAlertAction(title: "OK", style: .cancel)
                alert.addAction(cancelAction)
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func saveToLocal(ip: String, isp: String, time: String, typeConnect: String, download: Double, upload: Double, ping: Double, jitter: Double, lat: String, long: String, SSID: String, share: String, ipDevices: String, dateCreate: Date, testId: String?, namePoint: String, nameCity: String){
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "TestResult", in: managedContext)!
            let testResults = NSManagedObject(entity: entity, insertInto: managedContext)
            testResults.setValue(ip, forKeyPath: "ip")
            testResults.setValue(isp, forKeyPath: "isp")
            testResults.setValue(time, forKeyPath: "time")
            testResults.setValue(typeConnect, forKeyPath: "typeConnect")
            testResults.setValue(download, forKeyPath: "download")
            testResults.setValue(upload, forKeyPath: "upload")
            testResults.setValue(ping, forKeyPath: "ping")
            testResults.setValue(jitter, forKeyPath: "jitter")
            testResults.setValue(lat, forKeyPath: "latitude")
            testResults.setValue(long, forKeyPath: "lontitude")
            testResults.setValue(SSID, forKeyPath: "ssid")
            testResults.setValue(share, forKeyPath: "result")
            testResults.setValue(ipDevices, forKeyPath: "ipDevices")
            testResults.setValue(testId, forKeyPath: "testId")
            testResults.setValue(dateCreate, forKeyPath: "dateCreate")
            testResults.setValue(namePoint, forKeyPath: "namePoint")
            testResults.setValue(nameCity, forKeyPath: "nameCity")
            
            
            
            do {
                try managedContext.save()
                //self.testResult.append(testResults)
                print("testResultmanagedContext \(self.testResult.count)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func reset() {
        down = 0
        up = 0
        download = 0
        upload = 0
        ping = 0
        jitter = 0
    }
    
    @IBAction func testSpêdAgain(_ sender: Any) {
        reset()
        DispatchQueue.main.async { [self] in
            self.scrollView.setContentOffset(.zero, animated: true)
            self.scrollView.isScrollEnabled = false
            //self.heightContent.constant = 850
            self.btnShareResult.isEnabled = true
            self.commentTextfield.isEnabled = true
            commentTextfield.attributedPlaceholder =
            NSAttributedString(string: "Nhập bình luận của bạn", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6941176471, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
            self.ratingView.settings.updateOnTouch = true
            self.progressDownload.value = 0.0
            self.progressUpload.value = 0.0
            self.pingArr?.removeAll()
            self.valuePing.text = ""
            self.valueJitter.text = ""
            self.btnTestAgain.isHidden = true
            self.btnShare.isHidden = true
            self.ratingView.isHidden = true
            self.dtnShowLocation.isHidden = true
            self.textGuide.isHidden = true
            self.commentTextfield.isHidden = true
            self.btnShareResult.isHidden = true
            self.lbSpeedInter.isHidden = true
            //self.pageCollectionView.isHidden = true
            self.viewTextfield.isHidden = true
            self.StackThamKhao.isHidden = true
            //self.heightCollectionView.constant = 0
            //self.pageControl.isHidden = true
            //self.imageSpeedGuide.isHidden = true
            
        }
        self.ratingView.rating = 0
        //self.StartTest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.startTestPing(attempts: 10)
            //self.newTestPintStart(attempts: 10)
        }
    }
    
    @IBAction func back_action(_ sender: Any) {
        self.reset()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareResult(_ sender: Any) {
        let imageToShare = [self.urlShare]
        let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ .airDrop, .postToFacebook,.assignToContact, .mail, .message, .postToFlickr, .postToTwitter]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func generateRandomBytes(length: Int) -> [UInt8] {
        var randomBytes = [UInt8](repeating: 0, count: length)
        _ = SecRandomCopyBytes(kSecRandomDefault, length, &randomBytes)
        return randomBytes
    }
    
    //    func uploadFileWithMultithreading() {
    //
    //        let randomBytes = generateRandomBytes(length: 100 * 1000000)
    //
    //        let fileData = Data(randomBytes)
    //        let chunkSize = fileData.count / numberOfThreads
    //        let group = DispatchGroup()
    //        var totalBytesUploaded: Int64 = 0
    //        var startTime: DispatchTime?
    //
    //        for threadNumber in 0..<numberOfThreads {
    //            let startIndex = threadNumber * chunkSize
    //            let endIndex = (threadNumber == numberOfThreads - 1) ? fileData.count : startIndex + chunkSize
    //            let chunkData = fileData[startIndex..<endIndex]
    //
    //            group.enter()
    //
    //            DispatchQueue.global().async {
    //                let chunkStartTime = DispatchTime.now()
    //
    //                let request = self.createUploadRequest(data: chunkData)
    //
    //                let uploadTask = URLSession.shared.uploadTask(with: request, from: chunkData) { (_, _, error) in
    //                    if let error = error {
    //                        print("Upload failed with error: \(error)")
    //                    } else {
    //                        totalBytesUploaded += Int64(chunkData.count)
    //
    //                        if startTime == nil {
    //                            startTime = DispatchTime.now()
    //                        }
    //
    //                        let elapsedTime = Double(DispatchTime.now().uptimeNanoseconds - startTime!.uptimeNanoseconds) / 1_000_000_000.0
    //                        let uploadSpeed = Double(totalBytesUploaded) / elapsedTime / 1024 / 1024 // MB/s
    //
    //                        print("Thread \(threadNumber + 1): Uploaded \(chunkData.count / 1024) KB in \(elapsedTime) seconds. Speed: \(uploadSpeed) MB/s")
    //                        DispatchQueue.main.async {
    //                            self.progressLineUp.progress = Float(Double(totalBytesUploaded) / Double(chunkData.count)) / 100
    //                            self.upload = uploadSpeed
    //                            self.up = uploadSpeed
    //                            if uploadSpeed < 1000 {
    //                                if(uploadSpeed > 100) {
    //                                    self.progressUpload.maxValue = CGFloat(uploadSpeed)
    //                                } else {
    //                                    self.progressUpload.maxValue = 100.0
    //
    //                                }
    //                                self.progressUpload.progressAngle = 80
    //                                self.progressUpload.value = CGFloat(uploadSpeed)
    //                            }
    //                        }
    //                    }
    //
    //                    group.leave()
    //                }
    //
    //                uploadTask.resume()
    //
    //                let chunkElapsedTime = Double(DispatchTime.now().uptimeNanoseconds - chunkStartTime.uptimeNanoseconds) / 1_000_000_000.0
    //                print("Thread \(threadNumber + 1): Started upload in \(chunkElapsedTime) seconds.")
    //            }
    //        }
    //
    //        group.wait()
    //        print("Upload completed.")
    //        self.finishAllTask()
    //    }
    
    func createUploadRequest(data: Data) -> URLRequest {
        let uploadString = (pointCheck?.baseUrl ?? "") + "/" + (pointCheck?.uploadUrl ?? "")
        let serverURL = URL(string: uploadString)!
        var request = URLRequest(url: serverURL)
        request.httpMethod = "POST"
        // Configure the request as needed for your server and content type
        return request
    }
    
    func startTestPing(attempts: Int?) {
        // Reset the values.
        self.ping = 0
        self.jitter = 0
        
        self.attemptPings = attempts ?? Constants.Ping.attempts
        attemptPing = 1
        sessionPing = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: queuePing)
        pingServer()
        //return
    }
    
    private func pingServer() {
        let pingString = (pointCheck?.baseUrl ?? "") + "/" + (pointCheck?.pingUrl ?? "")
        guard let pingURL = URL(string: pingString) else { return }
        taskPing = sessionPing.dataTask(with: pingURL)
        taskPing.priority = URLSessionTask.highPriority
        
        startPing = Date.timeIntervalSinceReferenceDate
        taskPing.resume()
    }
    
    func newTestPintStart(attempts: Int?) {
        let pingString = (pointCheck?.baseUrl ?? "") + "/" + (pointCheck?.pingUrl ?? "")
        guard let pingURL = URL(string: pingString) else { return }
        let pingUtility = PingUtility(url: pingURL, attempts: attempts ?? 10)
        pingUtility.ping { [weak self] averagePing, jitter, error in
            var resultsText = ""
            
            if let averagePing = averagePing, let jitter = jitter {
                resultsText = "Average Ping: \(averagePing) ms\nJitter: \(jitter) ms\n"
            } else if let error = error {
                resultsText = "Failed to ping \(pingURL.absoluteString ?? ""): \(error.localizedDescription)\n"
            }
            
            DispatchQueue.main.async {
                self?.valuePing.text = String(averagePing ?? 0.0)
                self?.valueJitter.text = String(jitter ?? 0.0)
            }
            print("\(resultsText)")
            
        }
        let downloadString = (self.pointCheck?.baseUrl ?? "") + "/" + (self.pointCheck?.downloadUrl ?? "")
        self.startTestDownload(url: URL(string: downloadString)!)
    }
    
    private func updateValues() {
        let timeElapsed = Date.timeIntervalSinceReferenceDate - self.startPing
        let ping = self.ping
        let jitter = self.jitter
        
        let newJitter: Double
        if ping == 0 {
            newJitter = 0
        } else {
            let difference = abs((ping - timeElapsed) / 10.0)
            let priority: (old: Double, new: Double) = difference > jitter ? (0.3, 0.7) : (0.8, 0.2)
            newJitter = jitter * priority.old + difference * priority.new
            print("newJitter: \(newJitter)")
        }
        
        DispatchQueue.main.async {
            self.ping = timeElapsed
            self.jitter = newJitter
        }
    }
    
    func getResult() {
        guard let key = UserDefaults.standard.value(forKey: "RESULT") else {return}
        guard let urlPoint = URL(string: "https://api.i-speed.vn/v3/results/\(key)") else {return}
        let token = UserDefaults.standard.value(forKey: "TOKEN")
        let header: HTTPHeaders = [.authorization(bearerToken: token as! String)]
        
        AF.request(urlPoint , method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: header).responseJSON { [weak self] (response) in
            if let data = response.data {
                print(data)
                _ = try? JSON(data: response.data!)
                //print(json)
            }
        }
    }
    //MARK: set Index
    func finishAllTask() {
        debugPrint("Task completed1")
        let netInfo = CTTelephonyNetworkInfo()
        let carrier = netInfo.serviceSubscriberCellularProviders?.filter({ $0.value.carrierName != nil }).first?.value
        debugPrint("Task completed2: \(String(describing: carrier))")
        //self.managerment?.stopListening()
        //monitor.cancel()
        self.taskPing.cancel()
        self.getAllServer()
        
        var downloadQuality = 0, uploadQuality = 0, latencyQuality = 0;
        var videoQuality = 0, webQuality = 0, emailQuality = 0, imageSharingQuality = 0, gameQuality = 0, voiceQuality = 0

        /* QoE
        *  5: Excellent
        *  4: Good
        *  3: Moderate
        *  2: Poor
        *  1: Bad
        */

        // 1. video streaming quality
        // download
        if(self.download >= 25) {
            downloadQuality = 5;
        }
        else if (self.download >= 5)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 3)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 1)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 1.5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 0.5)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 0.1)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 0.05)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // latency
        if(self.ping <= 50) {
            latencyQuality = 5;
        }
        else if (self.ping <= 100)
        {
            latencyQuality = 4;
        }
        else if (self.ping <= 150)
        {
            latencyQuality = 3;
        }
        else if (self.ping <= 200)
        {
            latencyQuality = 2;
        }
        else {
            latencyQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        videoQuality = downloadQuality
        if (videoQuality >= uploadQuality) {videoQuality = uploadQuality}
        if (videoQuality >= latencyQuality) {videoQuality = latencyQuality}
        
        // 2. Web quality
        // download
        if(self.download >= 10) {
            downloadQuality = 5;
        }
        else if (self.download >= 5)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 3)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 1)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 0.5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 0.3)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 0.1)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 0.05)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // latency
        if(self.ping <= 50) {
            latencyQuality = 5;
        }
        else if (self.ping <= 100)
        {
            latencyQuality = 4;
        }
        else if (self.ping <= 150)
        {
            latencyQuality = 3;
        }
        else if (self.ping <= 200)
        {
            latencyQuality = 2;
        }
        else {
            latencyQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        webQuality = downloadQuality
        if (webQuality >= uploadQuality) {webQuality = uploadQuality}
        if (webQuality >= latencyQuality) {webQuality = latencyQuality}

        // 3. Email quality
        // download
        if(self.download >= 3) {
            downloadQuality = 5;
        }
        else if (self.download >= 1)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 0.5)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 0.1)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 0.5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 0.3)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 0.1)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 0.05)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        emailQuality = downloadQuality
        if (emailQuality >= uploadQuality) {emailQuality = uploadQuality}

        // 4. Image sharing quality
        // download
        if(self.download >= 10) {
            downloadQuality = 5;
        }
        else if (self.download >= 5)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 1.5)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 0.5)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 3)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 1.5)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 1)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        imageSharingQuality = downloadQuality
        if (imageSharingQuality >= uploadQuality) {imageSharingQuality = uploadQuality}

        // 5. Game quality
        // download
        if(self.download >= 10) {
            downloadQuality = 5;
        }
        else if (self.download >= 5)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 3)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 1)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 3)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 1)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 0.5)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // latency
        if(self.ping <= 50) {
            latencyQuality = 5;
        }
        else if (self.ping <= 100)
        {
            latencyQuality = 4;
        }
        else if (self.ping <= 150)
        {
            latencyQuality = 3;
        }
        else if (self.ping <= 200)
        {
            latencyQuality = 2;
        }
        else {
            latencyQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        gameQuality = downloadQuality
        if (gameQuality >= uploadQuality) {gameQuality = uploadQuality}
        if (gameQuality >= latencyQuality) {gameQuality = latencyQuality}

        // 6. Voice quality
        // download
        if(self.download >= 1.5) {
            downloadQuality = 5;
        }
        else if (self.download >= 1)
        {
            downloadQuality = 4;
        }
        else if (self.download >= 0.5)
        {
            downloadQuality = 3;
        }
        else if (self.download >= 0.2)
        {
            downloadQuality = 2;
        }
        else {
            downloadQuality = 1;
        }

        // upload
        if(self.upload >= 1.5) {
            uploadQuality = 5;
        }
        else if (self.upload >= 1)
        {
            uploadQuality = 4;
        }
        else if (self.upload >= 0.5)
        {
            uploadQuality = 3;
        }
        else if (self.upload >= 0.2)
        {
            uploadQuality = 2;
        }
        else {
            uploadQuality = 1;
        }

        // latency
        if(self.ping <= 50) {
            latencyQuality = 5;
        }
        else if (self.ping <= 100)
        {
            latencyQuality = 4;
        }
        else if (self.ping <= 150)
        {
            latencyQuality = 3;
        }
        else if (self.ping <= 200)
        {
            latencyQuality = 2;
        }
        else {
            latencyQuality = 1;
        }

        // overall quality is the smallest of download, upload, latency quality
        voiceQuality = downloadQuality
        if (voiceQuality >= uploadQuality) {voiceQuality = uploadQuality}
        if (voiceQuality >= latencyQuality) {voiceQuality = latencyQuality}
        
        DispatchQueue.main.async {
            self.scrollView.isScrollEnabled = true
            self.textGuide.isHidden = false
            self.btnTestAgain.isHidden = false
            self.btnShare.isHidden = false
            self.ratingView.isHidden = false
            self.dtnShowLocation.isHidden = false
            self.commentTextfield.isHidden = false
            self.btnShareResult.isHidden = false
            self.lbSpeedInter.isHidden = false
            self.viewTextfield.isHidden = false
            self.StackThamKhao.isHidden = false
            
          // Video Streaming
            if videoQuality == 5 {
                self.imgVideo.image = UIImage(named: "Excellent")
            } else if videoQuality == 4  {
                self.imgVideo.image = UIImage(named: "Good")
            }else if videoQuality == 3 {
                self.imgVideo.image = UIImage(named: "Normal")
            }else if videoQuality == 2 {
                self.imgVideo.image = UIImage(named: "NotGood")
            }else if videoQuality == 1 {
                self.imgVideo.image = UIImage(named: "Bad")
            }
            // Web
            if webQuality == 5 {
                self.imgWeb.image = UIImage(named: "Excellent")
            } else if webQuality == 4 {
                self.imgWeb.image = UIImage(named: "Good")
            }else if webQuality == 3 {
                self.imgWeb.image = UIImage(named: "Normal")
            }else if webQuality == 2 {
                self.imgWeb.image = UIImage(named: "NotGood")
            }else if webQuality == 1 {
                self.imgWeb.image = UIImage(named: "Bad")
            }
//            // email
//            
            if emailQuality == 5 {
                self.imgEmail.image = UIImage(named: "Excellent")
            } else if emailQuality == 4 {
                self.imgEmail.image = UIImage(named: "Good")
            }else if emailQuality == 3 {
                self.imgEmail.image = UIImage(named: "Normal")
            }else if emailQuality == 2 {
                self.imgEmail.image = UIImage(named: "NotGood")
            }else if emailQuality == 1 {
                self.imgEmail.image = UIImage(named: "Bad")
            }
//            
//            //share
//            
            if imageSharingQuality == 5 {
                self.imgShare.image = UIImage(named: "Excellent")
            } else if imageSharingQuality == 4 {
                self.imgShare.image = UIImage(named: "Good")
            }else if imageSharingQuality == 3 {
                self.imgShare.image = UIImage(named: "Normal")
            }else if imageSharingQuality == 2 {
                self.imgShare.image = UIImage(named: "NotGood")
            }else if imageSharingQuality == 1 {
                self.imgShare.image = UIImage(named: "Bad")
            }
//            
//            //GAME
//            
            if gameQuality == 5{
                self.imgGame.image = UIImage(named: "Excellent")
            } else if gameQuality == 4 {
                self.imgGame.image = UIImage(named: "Good")
            }else if gameQuality == 3 {
                self.imgGame.image = UIImage(named: "Normal")
            }else if gameQuality == 2 {
                self.imgGame.image = UIImage(named: "NotGood")
            }else if gameQuality == 1 {
                self.imgGame.image = UIImage(named: "Bad")
            }
//            
//            // CALL
            if voiceQuality == 5 {
                self.imgCall.image = UIImage(named: "Excellent")
            } else if voiceQuality == 4 {
                self.imgCall.image = UIImage(named: "Good")
            }else if voiceQuality == 3 {
                self.imgCall.image = UIImage(named: "Normal")
            }else if voiceQuality == 2 {
                self.imgCall.image = UIImage(named: "NotGood")
            }else if voiceQuality == 1 {
                self.imgCall.image = UIImage(named: "Bad")
            }
        }
        var ssid: String?
        if self.typeConnect == "WIFI" {
            ssid = FGRoute.getSSID()
            UserDefaults.standard.set(ssid, forKey: "SSID")
        } else {
            ssid = carrier?.carrierName
            UserDefaults.standard.set(ssid, forKey: "SSID")
        }
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy HH:mm"
        let dateString = df.string(from: date)
        debugPrint("Task completed")
        let testId = UserDefaults.standard.value(forKey: "RESULT") as? String
        self.createResult {
            self.getResult()
            let result = UserDefaults.standard.value(forKey: "urlShare") as? String
            self.saveToLocal(ip: self.ipAdress?.ip ?? "", isp: self.ipAdress?.isp ?? "",time: dateString, typeConnect: self.typeConnect ?? "", download: self.down, upload: self.up, ping: self.PingS ?? 0.0, jitter: self.jitter, lat: self.lat ?? "", long: self.long ?? "", SSID: ssid ?? "", share: result ?? "", ipDevices: self.getIPAddress(), dateCreate: date, testId: testId ?? "", namePoint: self.pointCheck?.name ?? "", nameCity: self.pointCheck?.city ?? "")
            //UserDefaults.standard.removeObject(forKey: "SSID")
        }
    }
    
    func createResult(completionHandler: @escaping () -> Void) {
        let clientCode = UserDefaults.standard.value(forKey: "CLIENT")
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = df.string(from: date)
        var bSsid: String?
        if self.typeConnect == "WIFI" {
            bSsid = FGRoute.getBSSID()
        }
        
        let ssid = UserDefaults.standard.value(forKey: "SSID") as? String
        guard let urlPoint = URL(string: "https://api.i-speed.vn/v3/results") else {return}
        let auth = UserDefaults.standard.value(forKey: "TOKEN")
        let headers: HTTPHeaders = [.authorization(bearerToken: auth as! String)]
        let params: Parameters = [
            "bssid": bSsid as Any,
            //"wifiSignalStrength": strengWifi as Any,
            "timestamp": dateString,
            "serverId": self.pointCheck?.id as Any,
            "download": self.down,
            "upload": self.up,
            "ping": self.PingS as Any,
            "jitter": self.jitter * 1000.0 as Any,
            "rating": "",
            "ipAddress": self.ipAdress?.ip as Any,
            "clientCode": clientCode as Any,
            "clientType": "MOBILE",
            "deviceCode": "",
            "deviceName": self.devicesName as Any,
            "osType": "iOS",
            "osVersion": UIDevice.current.systemVersion,
            "ssid": ssid as Any,
            "networkType": self.typeConnect as Any,
            "isp": self.ipAdress?.isp as Any,
            "asn": "",
            "city": self.pointCheck?.city as Any,
            "latitude": self.lat as Any,
            "longitude": self.long as Any
            
        ]
        print("param: \(params)")
        AF.request(urlPoint , method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { [weak self] (response) in
            if let urlShare = response.response?.allHeaderFields["Location"] as? String {
                print(urlShare)
                self?.url = urlShare
                UserDefaults.standard.setValue(urlShare, forKey: "urlShare")
            }
            if response.error == nil {
                let json = try? JSON(data: response.data!)
                let name = json?["message"].stringValue
                let resultCode = name?.dropFirst(27)
                let defaults = UserDefaults.standard
                defaults.set(resultCode, forKey: "RESULT")
                completionHandler()
                print(resultCode as Any)
            }else {
                print(response.error?.localizedDescription as Any)
            }
            
        }
    }
    //        func downloadFileWithMultithreading(url: String) {
    //            guard let url = URL(string: url) else {
    //                print("Invalid URL")
    //                return
    //            }
    //
    //            let fileURL = url.lastPathComponent
    //            let destinationURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(fileURL)
    //
    //            let concurrentQueue = DispatchQueue(label: "com.example.concurrentQueue", attributes: .concurrent)
    //            let group = DispatchGroup()
    //
    //            let chunkSize = 100 * 1024 * 1024 // 1 MB
    //            let totalChunks = 10 // Number of concurrent download chunks
    //
    //            let fileSize = chunkSize * totalChunks
    //            var totalDownloadedSize: Int64 = 0
    //            var startTime: DispatchTime?
    //
    //            for chunk in 0..<totalChunks {
    //                let chunkStart = chunk * chunkSize
    //                let chunkEnd = chunkStart + chunkSize - 1
    //                let requestRange = "bytes=\(chunkStart)-\(chunkEnd)"
    //
    //                concurrentQueue.async(group: group) {
    //                    group.enter()
    //
    //                    var request = URLRequest(url: url)
    //                    request.setValue(requestRange, forHTTPHeaderField: "Range")
    //
    //                    let downloadTask = URLSession.shared.downloadTask(with: request) { (url, response, error) in
    //                        defer { group.leave() }
    //
    //                        if let error = error {
    //                            print("Download failed with error: \(error)")
    //                            return
    //                        }
    //
    //                        if let url = url {
    //                            do {
    //                                let chunkData = try Data(contentsOf: url)
    //                                try chunkData.write(to: destinationURL, options: .atomic)
    //
    //                                DispatchQueue.main.async {
    //                                    totalDownloadedSize += Int64(chunkData.count)
    //
    //                                    if startTime == nil {
    //                                        startTime = DispatchTime.now()
    //                                    }
    //
    //                                    let elapsedTime = DispatchTime.now().uptimeNanoseconds - startTime!.uptimeNanoseconds
    //                                    let elapsedTimeInSeconds = Double(elapsedTime) / 1_000_000_000.0
    //
    //                                    let downloadSpeed = Double(totalDownloadedSize) / elapsedTimeInSeconds / 1024 / 1024 // MB/s
    //                                    print("Download Speed: \(downloadSpeed) MB/s")
    //                                    DispatchQueue.main.async {
    //                                        self.progressLineDown.progress = Float(Double(totalDownloadedSize) / Double(chunkData.count)) / 100
    //                                        self.download = downloadSpeed
    //                                        self.down = downloadSpeed
    //                                        if downloadSpeed < 1000 {
    //                                            if(downloadSpeed > 100) {
    //                                                self.progressDownload.maxValue = CGFloat(downloadSpeed)
    //                                            } else {
    //                                                self.progressDownload.maxValue = 100.0
    //                                            }
    //                                            self.progressDownload.progressAngle = 80
    //                                            self.progressDownload.value = CGFloat(downloadSpeed)
    //
    //                                        }
    //
    //                                    }
    //                                }
    //                            } catch {
    //                                print("Error: \(error)")
    //                            }
    //                        }
    //                    }
    //
    //                    downloadTask.resume()
    //                }
    //            }
    //
    //            group.notify(queue: DispatchQueue.global()) {
    //                print("Download completed.")
    //                self.uploadFileWithMultithreading()
    //            }
    //        }
    func startTestUpload(){
        var bytes = [UInt8]()
        if typeConnect == "WIFI" || typeConnect == "5G" {
            bytes = [UInt8](repeating: 0, count: 300*1048576)
        } else {
            bytes = [UInt8](repeating: 0, count: 100*1048576)
        }
        let status = SecRandomCopyBytes(kSecRandomDefault, bytes.count, &bytes)
        
        dummyData = Data(bytes)
        print("dummy:\(dummyData.count)")
        
        guard status == errSecSuccess else {
            upload = 0
            return
        }
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = TimeInterval(15)
        config.timeoutIntervalForResource = TimeInterval(15)
        sessionUpload = URLSession(configuration: config, delegate: self, delegateQueue: queueUpload)
        uploadToServer()
    }
    
    private func uploadToServer() {
        let uploadString = (pointCheck?.baseUrl ?? "") + "/" + (pointCheck?.uploadUrl ?? "")
        print("uploadString:\(uploadString)")
        guard let uploadURL = URL(string: uploadString) else { return }
        var request = URLRequest(url: uploadURL)
        request.httpMethod = "POST"
        taskUpload = sessionUpload.uploadTask(with: request, from: dummyData)
        taskUpload.priority = URLSessionTask.highPriority
        
        startUpload = Date.timeIntervalSinceReferenceDate
        taskUpload.resume()
    }
    
    func startTestDownload(url: URL) {
        download = 0
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = TimeInterval(15)
        config.timeoutIntervalForResource = TimeInterval(15)
        sessionDownload = URLSession(configuration: config, delegate: self, delegateQueue: queueDownload)
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            download = 0
            return
        }
        if typeConnect == "WIFI" || typeConnect == "5G" {
            urlComponents.queryItems = [URLQueryItem(name: "ckSize", value: String(300))]
        } else {
            urlComponents.queryItems = [URLQueryItem(name: "ckSize", value: String(100))]
        }
        
        guard let finalUrl = urlComponents.url else {
            download = 0
            return
        }
        taskDownload = sessionDownload.downloadTask(with: finalUrl)
        taskDownload.priority = URLSessionTask.highPriority
        startDownload = Date.timeIntervalSinceReferenceDate
        taskDownload.resume()
        taskPing.cancel()
    }
}

extension SpeedTestVC: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        sessionDownload.invalidateAndCancel()
        DispatchQueue.main.async {
            self.progressLineDown.setProgress(1.0, animated: true)
        }
        
        self.startTestUpload()
        
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let timeElapsed = Date.timeIntervalSinceReferenceDate - startDownload
        let speedB = Double(totalBytesWritten) / timeElapsed
        let speed = (8 * speedB * Constants.overheadCompensationFactor) / Double(Constants.bytesPerMegaByte)
        DispatchQueue.main.async {
            if self.typeConnect == "WIFI" || self.typeConnect == "5G" {
                self.progressLineDown.progress = Float(totalBytesWritten) / Float(300*1048576)
            } else {
                self.progressLineDown.progress = Float(totalBytesWritten) / Float(100*1048576)
            }
            
        }
        
        DispatchQueue.main.async {
            self.download = speed
            self.down = speed
            if(speed > 100) {
                self.progressDownload.maxValue = CGFloat(speed)
            } else {
                self.progressDownload.maxValue = 100.0
                
            }
            self.progressDownload.progressAngle = 80
            self.progressDownload.value = CGFloat(speed)
        }
    }
}

extension SpeedTestVC: URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if task == self.taskDownload {
            if let error = error {
                if error.localizedDescription == "The request timed out." {
                    taskDownload.cancel()
                    self.startTestUpload()
                }
            }
        }
        if task == self.taskUpload {
            if let error = error {
                if error.localizedDescription == "The request timed out." {
                    taskUpload.cancel()
                    self.finishAllTask()
                }
            } else {
                if attemptUpload < Constants.Upload.attempts {
                    attemptUpload += 1
                    DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + Constants.Upload.requestPadding) {
                        self.uploadToServer()
                    }
                } else {
                    // Finish
                    self.finishAllTask()
                }
            }
        }
        else if task == self.taskPing {
            if let error = error {
                print(error.localizedDescription)
                self.ping = 0
                self.jitter = 0
            } else {
                updateValues()
                if attemptPing < attemptPings {
                    attemptPing += 1
                    DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + Constants.Ping.requestPadding) {
                        self.pingServer()
                    }
                } else {
                    let downloadString = (self.pointCheck?.baseUrl ?? "") + "/" + (self.pointCheck?.downloadUrl ?? "")
                    guard let downloadURL = URL(string: downloadString) else { return }
                    self.startTestDownload(url: downloadURL)
                    
                }
            }
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let timeElapsed = Date.timeIntervalSinceReferenceDate - startUpload
        let speedB = Double(totalBytesSent) / timeElapsed
        let speed = 8 * speedB * Constants.overheadCompensationFactor / Double(Constants.bytesPerMegaByte)
        DispatchQueue.main.async {
            self.progressLineUp.progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        }
        
        DispatchQueue.main.async {
            self.upload = speed
            self.up = speed
            if(speed > 100) {
                self.progressUpload.maxValue = CGFloat(speed)
            } else {
                self.progressUpload.maxValue = 100.0
                
            }
            self.progressUpload.progressAngle = 80
            self.progressUpload.value = CGFloat(speed)
            
            //task.cancel()
        }
    }
}

extension SpeedTestVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pageCollectionView.dequeueReusableCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        //if (self.colors.count > 4) {
        //cell.imageGuide.image = self.colors[indexPath.item]
        
        //}
        
        return cell
    }
}




extension UIViewController {
    func dismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer( target:     self, action:    #selector(UIViewController.dismissKeyboardTouchOutside))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboardTouchOutside() {
        view.endEditing(true)
    }
}


