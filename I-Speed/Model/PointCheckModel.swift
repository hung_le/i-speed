//
//  PointCheckModel.swift
//  I-Speed
//
//  Created by Le Hung on 3/3/21.
//

import Foundation
import Alamofire

struct PointCheckModel: Decodable, Equatable {
    var id: Int?
    var name: String?
    var city: String?
    var distance: String!
    var baseUrl: String?
    var downloadUrl: String?
    var uploadUrl: String?
    var pingUrl: String?
    var pingValue: Int!
    
    
    init(id: Int?,name: String?, city: String?, distance: String!, baseUrl: String?, downloadUrl: String?, uploadUrl: String?,pingUrl: String?) {
        self.id = id
        self.name = name
        self.city = city
        self.distance = distance
        self.baseUrl = baseUrl
        self.downloadUrl = downloadUrl
        self.uploadUrl = uploadUrl
        self.pingUrl = pingUrl
    }
    
    init() {
        
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case city = "city"
        case distance = "distance"
        case baseUrl = "baseUrl"
        case downloadUrl = "downloadUrl"
        case uploadUrl = "uploadUrl"
        case pingUrl = "pingUrl"
    }
}
