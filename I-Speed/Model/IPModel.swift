//
//  IPModel.swift
//  I-Speed
//
//  Created by Le Hung on 3/17/21.
//

import Foundation
import Alamofire

struct IPModel: Decodable {
    var ip: String?
    var isp: String?
    
    enum CodingKeys: String, CodingKey {
        case ip = "ip"
        case isp = "isp"
        
    }
}


