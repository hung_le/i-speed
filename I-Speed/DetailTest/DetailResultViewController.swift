//
//  DetailResultViewController.swift
//  I-Speed
//
//  Created by Le Hung on 3/21/21.
//

import UIKit
import CoreData
import Network
import FGRoute
import CoreTelephony

class DetailResultViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    let info: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
    
    var testResultDetail: NSManagedObject?
    var managedObjectContext:NSManagedObjectContext!
    
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var ssid: UILabel!
    @IBOutlet weak var isp: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var download: UILabel!
    @IBOutlet weak var upload: UILabel!
    @IBOutlet weak var ping: UILabel!
    @IBOutlet weak var jitter: UILabel!
    @IBOutlet weak var nameNetwork: UILabel!
    @IBOutlet weak var nameDevices: UILabel!
    @IBOutlet weak var nameServer: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var typeConnect: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var IpAddess: UILabel!
    @IBOutlet weak var ipLocal: UILabel!
    @IBOutlet weak var iconLocation: UIImageView!
    @IBOutlet weak var btnShowMap: UIButton!
    @IBOutlet weak var iconNetwork: UIImageView!
    @IBOutlet weak var ipInternal: UILabel!
    @IBOutlet weak var testId: UILabel!
    
    @IBOutlet weak var locationTest: UILabel!
    @IBOutlet weak var nameSsid: UILabel!
    @IBOutlet weak var titleSsid: UILabel!
    @IBOutlet weak var ipV4: UILabel!
    @IBOutlet weak var iconIP: UIImageView!
    var ip: String?
    var netWork: String?
    var lat: String?
    var long: String?
    var devices: String?{
        didSet {
            self.nameDevices.text = self.devices
        }
    }
    var nameSer: String?
//    {
//        didSet {
//            self.nameServer.text = self.nameSer
//        }
//    }
    var cityName: String?
//    {
//        didSet {
//            self.city.text = self.cityName
//        }
//    }
    var typeCon: String?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @IBAction func btnShare(_ sender: Any) {
        guard let urlShare = testResultDetail?.value(forKeyPath: "result") as? String else {return}
        let imageToShare = [urlShare]
        let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ .airDrop, .postToFacebook,.assignToContact, .mail, .message, .postToFlickr, .postToTwitter]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func deleteItem(_ sender: Any) {
        let alert = UIAlertController(title: "Xoá bản ghi", message: "Bản ghi sẽ được xoá khỏi lịch sử đo", preferredStyle: UIAlertController.Style.alert)
        let agree = UIAlertAction(title: "Đồng ý", style: .default) {[weak self] (_) -> Void in
            if let testResult = self?.testResultDetail {
                self?.managedObjectContext.delete(testResult)
                do {
                    try self?.managedObjectContext.save()
                } catch {
                }
                DispatchQueue.main.async {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
        let degree = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(agree)
        alert.addAction(degree)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //    func getIPAddress() -> String {
    //       var address: String?
    //       var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    //       if getifaddrs(&ifaddr) == 0 {
    //           var ptr = ifaddr
    //           while ptr != nil {
    //               defer { ptr = ptr?.pointee.ifa_next }
    //
    //               let interface = ptr?.pointee
    //               let addrFamily = interface?.ifa_addr.pointee.sa_family
    //               if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
    //
    //                   // wifi = ["en0"]
    //                   // wired = ["en2", "en3", "en4"]
    //                   // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]
    //
    //                   let name: String = String(cString: (interface!.ifa_name))
    //                   if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
    //                       var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
    //                       getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
    //                       address = String(cString: hostname)
    //                   }
    //               }
    //           }
    //           freeifaddrs(ifaddr)
    //       }
    //       return address ?? ""
    //   }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBorder.layer.cornerRadius = 16
        viewBorder.clipsToBounds = true
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        if let testId = testResultDetail?.value(forKey: "testId") as? String {
            //self.testId.text = "TestID: \(testId.uppercased())"
            
        }
        if let ipLocal = testResultDetail?.value(forKeyPath: "ipDevices") as? String {
            self.ipInternal.text = "Thiết bị: \(ipLocal)"
        }
        if let nameSer = testResultDetail?.value(forKeyPath: "namePoint") as? String {
            self.nameServer.text = nameSer
        }
        if let nameCity = testResultDetail?.value(forKeyPath: "nameCity") as? String {
            self.city.text = nameCity
        }
        
        self.time.text = testResultDetail?.value(forKeyPath: "time") as? String
        
        if let down = testResultDetail?.value(forKeyPath: "download") as? Double, let up = testResultDetail?.value(forKeyPath: "upload") as? Double, let ping = testResultDetail?.value(forKeyPath: "ping") as? Double, let jitter = testResultDetail?.value(forKeyPath: "jitter") as? Double {
            self.download.text = String(format: "%.1f", locale: Locale(identifier: "de"), down)
            self.upload.text = String(format: "%.1f", locale: Locale(identifier: "de"), up)
            self.ping.text = String(format: "%.1f", locale: Locale(identifier: "de"), (ping))
            self.jitter.text = String(format: "%.2f", locale: Locale(identifier: "de"), (jitter * 1000.0))
        }
        //        self.download.text = String(format:"%.1f",(testResultDetail?.value(forKeyPath: "download") as? Double)!)
        //        self.upload.text = String(format:"%.1f",(testResultDetail?.value(forKeyPath: "upload") as? Double)!)
        //        self.ping.text = String(format:"%.f",(((testResultDetail?.value(forKeyPath: "ping") as? Double)!) * 1000.0))
        //        self.jitter.text = String(format:"%.2f", locale: Locale(identifier: "de"),(((testResultDetail?.value(forKeyPath: "jitter") as? Double)!) * 1000.0))
        if let ssid = testResultDetail?.value(forKey: "ssid") as? String {
            self.ssid.text = "\(ssid)"
            if ssid == "" {
                self.ssid.text = "Không lấy được thông tin SSID"
            }
        }
        if let type = testResultDetail?.value(forKeyPath: "typeConnect") as? String {
            if type == "WIFI" {
                titleSsid.text = "SSID:"
            } else {
                titleSsid.text = "Nhà mạng di động:"
            }
        }
        guard let text = testResultDetail?.value(forKeyPath: "typeConnect") as? String else {return}
        if text == "LTE" {
            self.iconNetwork.image = UIImage(named: "LTE")
        }else if text == "3G" {
            self.iconNetwork.image = UIImage(named: "3G")
        }else if text == "WIFI" {
            self.iconNetwork.image = UIImage(named: "icon-wifi-2")
            
        }else if text == "5G" {
            self.iconNetwork.image = UIImage(named: "5G")
        }else if text == "2G" {
            self.iconNetwork.image = UIImage(named: "2G")
        } 
        if let ip = testResultDetail?.value(forKeyPath: "ip") as? String {
            self.IpAddess.text = "Cổng kết nối: \(ip)"
            if let _ = IPv4Address(ip) {
                //IP v4
                self.ipV4.text = ""
            } else if let _ = IPv6Address(ip) {
                self.ipV4.text = ""
                
                // IP v6
            }
            
        }
        //self.IpAddess.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //print("POPO\(testResultDetail?.value(forKeyPath: "isp") as? String)")
        if let nameNet = testResultDetail?.value(forKeyPath: "isp") as? String {
            self.nameNetwork.text = nameNet
        }
        
        if let lat = testResultDetail?.value(forKeyPath: "latitude") as? String, let long = testResultDetail?.value(forKeyPath: "lontitude") as? String {
            if lat != "" || long != "" {
                DispatchQueue.main.async {
                    self.locationTest.isHidden = false
                    self.btnShowMap.isHidden = false
                    //self.iconLocation.isHidden = false
                    self.location.isHidden = false
                }
                let dms = self.coordinateToDMS(latitude: Double(lat)!, longitude: Double(long)!)
                self.location.text = "Vĩ độ : \(dms.latitude) \nKinh độ : \(dms.longitude)"
            } else {
                DispatchQueue.main.async {
                    self.locationTest.isHidden = true
                    self.btnShowMap.isHidden = true
                    //self.iconLocation.isHidden = true
                    self.location.isHidden = true
                }
            }
        }
    }
    
    @IBAction func explainButton(_ sender: Any) {
        let vc = IntroVC()
        vc.id = 2
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openLocation(_ sender: Any) {
        guard let lat = self.lat, let latDouble =  Double(lat) else { return }
        guard let long = self.long, let longDouble =  Double(long) else { return }
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
            
            if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(latDouble),\(longDouble)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }}
        else {
            //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(latDouble),\(longDouble)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //ip = UserDefaults.standard.value(forKey: "IPADDRESS") as? String
        //netWork = UserDefaults.standard.value(forKey: "NETWORK") as? String
        self.lat = UserDefaults.standard.value(forKey: "LATITUDE") as? String
        self.long = UserDefaults.standard.value(forKey: "LONGITUDE") as? String
        devices = UserDefaults.standard.value(forKey: "DEVICES") as? String
        nameSer = UserDefaults.standard.value(forKey: "NAME_NET") as? String
        cityName = UserDefaults.standard.value(forKey: "CITY") as? String
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    func coordinateToDMS(latitude: Double, longitude: Double) -> (latitude: String, longitude: String) {
        let latDegrees = abs(Int(latitude))
        let latMinutes = abs(Int((latitude * 3600).truncatingRemainder(dividingBy: 3600) / 60))
        let latSeconds = Double(abs((latitude * 3600).truncatingRemainder(dividingBy: 3600).truncatingRemainder(dividingBy: 60)))
        
        let lonDegrees = abs(Int(longitude))
        let lonMinutes = abs(Int((longitude * 3600).truncatingRemainder(dividingBy: 3600) / 60))
        let lonSeconds = Double(abs((longitude * 3600).truncatingRemainder(dividingBy: 3600).truncatingRemainder(dividingBy: 60) ))
        
        return (String(format:"%d° %d' %.4f\" %@", latDegrees, latMinutes, latSeconds, latitude >= 0 ? "N" : "S"),
                String(format:"%d° %d' %.4f\" %@", lonDegrees, lonMinutes, lonSeconds, longitude >= 0 ? "E" : "W"))
    }
}


