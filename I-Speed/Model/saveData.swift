//
//  saveData.swift
//  I-Speed
//
//  Created by Le Hung on 4/7/22.
//

import Foundation

struct Point:Codable{
    
    var id:Int?
    var name:String?
    var city:String?
    var distance:String?
    var baseUrl: String?
    var downloadUrl: String?
    var uploadUrl: String?
    var pingUrl: String?
    var pingValue: Int?
}
