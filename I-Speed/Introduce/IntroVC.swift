//
//  IntroVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/25/21.
//

import UIKit

public protocol ChangableFont: AnyObject {
    var rangedAttributes: [RangedAttributes] { get }
    func getText() -> String?
    func set(text: String?)
    func getAttributedText() -> NSAttributedString?
    func set(attributedText: NSAttributedString?)
    func getFont() -> UIFont?
    func changeFont(ofText text: String, with font: UIFont)
    func changeFont(inRange range: NSRange, with font: UIFont)
    func changeTextColor(ofText text: String, with color: UIColor)
    func changeTextColor(inRange range: NSRange, with color: UIColor)
    func resetFontChanges()
}

public struct RangedAttributes {
    
    public let attributes: [NSAttributedString.Key: Any]
    public let range: NSRange
    
    public init(_ attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        self.attributes = attributes
        self.range = range
    }
}

class IntroVC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    var contentString: String?
    var id: Int?
    
    @IBOutlet weak var content: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if id == 1 {
            let helloWorldString = "GIỚI THIỆU, ĐIỀU KHOẢN SỬ DỤNG ỨNG DỤNG ĐO TỐC ĐỘ TRUY CẬP INTERNET VIỆT NAM (i-SPEED BY VNNIC)\n\ni-Speed by VNNIC (i-Speed) là ứng dụng đo tốc độ truy cập Internet Việt Nam do Trung tâm Internet Việt Nam (VNNIC) thuộc Bộ Thông tin và Truyền thông xây dựng, phát triển. Người sử dụng vui lòng đọc kỹ Điều khoản sử dụng trước khi tiến hành cài đặt, sử dụng bất kỳ phần nào của ứng dụng (bao gồm nhưng không giới hạn các phần mềm, các file và các tài liệu liên quan). Người dùng chấp thuận và đồng ý thực hiện các quy định và điều kiện trong Điều khoản này khi thực hiện các thao tác trên đây. \n\nĐiều 1. Giới thiệu về ứng dụng\n\nỨng dụng i-Speed giúp người dùng Internet Việt Nam có thể tự kiểm tra, tự đánh giá tốc độ truy cập Internet với các thông số: tốc độ tải xuống (Download), tốc độ tải lên (Upload), trễ (Ping, Jitter).\n\nLà hệ thống đo trung lập, i-Speed phản ánh trung thực kết quả đo tốc độ truy cập Internet của người dùng. Dữ liệu đo sẽ đóng góp hỗ trợ cơ quan quản lý xây dựng, phát triển hạ tầng, dịch vụ Internet tại Việt Nam; nâng cao chất lượng dịch vụ của doanh nghiệp cung cấp dịch vụ Internet; bảo vệ quyền lợi người sử dụng Internet.\n\nNgười dùng có thể cài đặt ứng dụng trên thiết bị điện thoại di động, máy tính bảng dùng hệ điều hành Android, iOS (Apple) hoặc thực hiện đo trực tiếp trên trang web https://speedtest.vn.\n\nĐiều 2. Cập nhật\n\nỨng dụng này có thể được cập nhật thường xuyên bởi Trung tâm Internet Việt Nam. Phiên bản cập nhật sẽ được chúng tôi công bố chính thức tại Website https://speedtest.vn .Phiên bản cập nhật sẽ thay thế cho các quy định và điều khoản ban đầu. Người dùng có thể truy cập vào ứng dụng hoặc Website trên để xem nội dung chi tiết của phiên bản cập nhật.\n\nĐiều 3. Quyền truy cập, thu thập thông tin và bảo vệ sự riêng tư\n\nKhi sử dụng công cụ đo tốc độ truy cập Internet Việt Nam trên trên Website ( https://speedtest.vn , https://i-speed.vn ) và trên Ứng dụng i-Speed, người dùng sẽ được hỏi về việc cho phép truy cập về vị trí địa lý, quyền quản lý cuộc gọi của thiết bị sử dụng. Người dùng có thể đồng ý hoặc không đồng ý. Dữ liệu vị trí phục vụ chọn điểm đo tối ưu nhất, thông tin khoảng cách đến các điểm đo và phục vụ thống kê theo địa phương. Ứng dụng chỉ dùng quyền quản lý cuộc gọi để lấy thông tin chính xác về kết nối mạng di động, dịch vụ 5G, nhà cung cấp dịch vụ.\n\nNgoài dữ liệu về vị trí địa lý, khi sử dụng Ứng dụng i-Speed, người dùng đồng ý cho phép ứng dụng có quyền truy cập vào những dữ liệu sau: Thông tin về thiết bị di động cài ứng dụng (hệ điều hành, phiên bản hệ điều hành, chủng loại của thiết bị); Thông số kỹ thuật kết nối mạng Internet.\n\nĐiều 4. Quyền, trách nhiệm của người dùng\n\nĐược bảo vệ thông tin người dùng theo quy định của pháp luật.\n\nTrong quá trình sử dụng ứng dụng, người dùng được sử dụng các dịch vụ hỗ trợ theo quy định. Khi phát hiện ra lỗi của sản phẩm, người dùng hãy thông báo cho chúng tôi.\n\nThực hiện quyền và trách nhiệm khác theo quy định pháp luật.\nMọi sự hỗ trợ xin liên hệ:\nTrung tâm Internet Việt Nam (VNNIC).\nWebsite: https://vnnic.vn\nEmail: i-speed@vnnic.vn."
            content.text = helloWorldString
            content.font = UIFont.init(name: "Roboto", size: 16.0)
            content.textColor = UIColor.white
            content.changeFont(ofText: "GIỚI THIỆU, ĐIỀU KHOẢN SỬ DỤNG ỨNG DỤNG ĐO TỐC ĐỘ TRUY CẬP INTERNET VIỆT NAM (i-SPEED BY VNNIC)", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Điều 1. Giới thiệu về ứng dụng", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Điều 2. Cập nhật", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Điều 3. Quyền truy cập, thu thập thông tin và bảo vệ sự riêng tư", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Điều 4. Quyền, trách nhiệm của người dùng", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeTextColor(ofText: "https://i-speed.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            content.changeTextColor(ofText: "https://speedtest.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            content.changeTextColor(ofText: "https://vnnic.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            content.changeTextColor(ofText: "i-speed@vnnic.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
//            content.changeTextColor(ofText: "https://speedtest.vn.", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
//            content.changeTextColor(ofText: "https://speedtest.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            
            
        }
        if id == 2 {
            let text = "Các thông số phản ánh tốc độ truy cập Internet bao gồm: tốc độ tải xuống (Download), tốc độ tải lên (Upload), thời gian trễ (Ping, Jitter: đo qua giao thức http).\n\nTốc độ Download:\n\nLà tốc độ lấy, tải dữ liệu từ điểm đo đến thiết bị người dùng tại thời điểm kiểm tra. Tốc độ download càng cao thì việc tải dữ liệu từ Internet về máy người dùng càng nhanh, phù hợp với việc xem tin tức, phim, video clip trực tuyến, tải tài liệu, tải file, tải thư điện tử (email).\n\nĐơn vị đo là Mbps.\n\nTốc độ Upload:\n\nLà tốc độ đẩy, tải dữ liệu lên từ thiết bị người dùng tới điểm đo tại thời điểm kiểm tra. Tốc độ upload càng cao thì việc đẩy dữ liệu từ thiết bị của người dùng lên Internet càng nhanh, phù hợp với việc gửi file, tài liệu, video clip, gửi thư điện tử (email).\n\nĐơn vị đo là Mbps.\n\nThời gian trễ Ping, Jitter:\n\nPing và Jitter là hai thông số rất quan trọng với các dịch vụ thoại, video, video games … trên Internet, nếu 2 tham số này cao thì sẽ gây ra hiện tượng méo tiếng và vỡ hình.\n\nĐơn vị đo là ms (mili giây).\n\nThông tin chi tiết về tốc độ khuyến nghị với các loại ứng dụng, truy cập tại https://speedtest.vn/thong-so-do"
            content.text = text
            content.font = UIFont.init(name: "Roboto", size: 16.0)
            content.textColor = UIColor.white
            content.changeFont(ofText: "Tốc độ Download:", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Tốc độ Upload:", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeFont(ofText: "Thời gian trễ Ping, Jitter:", with: UIFont.init(name: "Roboto-Bold", size: 16.0)!)
            content.changeTextColor(ofText: "https://speedtest.vn/thong-so-do", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            
            
        }
        if id == 3 {
            let textContent = "Mọi sự hỗ trợ xin liên hệ:\nTrung tâm Internet Việt Nam (VNNIC)\nWebsite: https://vnnic.vn\nEmail:  i-speed@vnnic.vn."
            content.text = textContent
            content.font = UIFont.init(name: "Roboto", size: 16.0)
            content.textColor = UIColor.white
            content.changeTextColor(ofText: "https://vnnic.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            content.changeTextColor(ofText: "i-speed@vnnic.vn", with: #colorLiteral(red: 0.4784313725, green: 0.8745098039, blue: 1, alpha: 1))
            
        }
        if id == 4 {
            if let appVersion = Bundle.main.releaseVersionNumber {
                content.text = "Phiên bản ứng dụng hiện tại : \(appVersion)"
                content.font = UIFont.init(name: "Roboto", size: 16.0)
                content.textColor = UIColor.white
            }
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        contentString?.removeAll()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setGradientBackground()
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

extension UILabel: ChangableFont {
    
    public func getText() -> String? {
        return text
    }
    
    public func set(text: String?) {
        self.text = text
    }
    
    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }
    
    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }
    
    public func getFont() -> UIFont? {
        return font
    }
}

extension UITextField: ChangableFont {
    
    public func getText() -> String? {
        return text
    }
    
    public func set(text: String?) {
        self.text = text
    }
    
    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }
    
    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }
    
    public func getFont() -> UIFont? {
        return font
    }
}

extension UITextView: ChangableFont {
    
    public func getText() -> String? {
        return text
    }
    
    public func set(text: String?) {
        self.text = text
    }
    
    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }
    
    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }
    
    public func getFont() -> UIFont? {
        return font
    }
    func addHyperLinksToText(originalText: String, hyperLinks: [String: String]) {
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        for (hyperLink, urlString) in hyperLinks {
            let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
            let fullRange = NSRange(location: 0, length: attributedOriginalText.length)
            attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
            attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
            attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18.0), range: fullRange)
        }
        
        self.linkTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.systemTeal,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
        ]
        self.attributedText = attributedOriginalText
    }
}


public extension ChangableFont {
    
    var rangedAttributes: [RangedAttributes] {
        guard let attributedText = getAttributedText() else {
            return []
        }
        var rangedAttributes: [RangedAttributes] = []
        let fullRange = NSRange(
            location: 0,
            length: attributedText.string.count
        )
        attributedText.enumerateAttributes(
            in: fullRange,
            options: []
        ) { (attributes, range, stop) in
            guard range != fullRange, !attributes.isEmpty else { return }
            rangedAttributes.append(RangedAttributes(attributes, inRange: range))
        }
        return rangedAttributes
    }
    
    func changeFont(ofText text: String, with font: UIFont) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeFont(inRange: range, with: font)
    }
    
    func changeFont(inRange range: NSRange, with font: UIFont) {
        add(attributes: [.font: font], inRange: range)
    }
    
    func changeTextColor(ofText text: String, with color: UIColor) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeTextColor(inRange: range, with: color)
    }
    
    func changeTextColor(inRange range: NSRange, with color: UIColor) {
        add(attributes: [.foregroundColor: color], inRange: range)
    }
    
    private func add(attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        guard !attributes.isEmpty else { return }
        
        var rangedAttributes: [RangedAttributes] = self.rangedAttributes
        
        var attributedString: NSMutableAttributedString
        
        if let attributedText = getAttributedText() {
            attributedString = NSMutableAttributedString(attributedString: attributedText)
        } else if let text = getText() {
            attributedString = NSMutableAttributedString(string: text)
        } else {
            return
        }
        
        rangedAttributes.append(RangedAttributes(attributes, inRange: range))
        
        rangedAttributes.forEach { (rangedAttributes) in
            attributedString.addAttributes(
                rangedAttributes.attributes,
                range: rangedAttributes.range
            )
        }
        
        set(attributedText: attributedString)
    }
    
    func resetFontChanges() {
        guard let text = getText() else { return }
        set(attributedText: NSMutableAttributedString(string: text))
    }
}

public extension String {
    
    func range(ofText text: String) -> NSRange {
        let fullText = self
        let range = (fullText as NSString).range(of: text)
        return range
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

