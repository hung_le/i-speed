//
//  SplashVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/3/21.
//

import UIKit
import Alamofire
import CoreLocation
import NetworkExtension
import UIKit
import SystemConfiguration.CaptiveNetwork
import FGRoute
//import SwiftyJSON

class SplashVC: UIViewController, CLLocationManagerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    @IBOutlet weak var viewOut: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
}
