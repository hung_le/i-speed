//
//  PopupIPVC.swift
//  I-Speed
//
//  Created by mac on 16/09/2023.
//

import UIKit

class PopupIPV6VC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ip?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbv.dequeueReusableCell(withIdentifier: "ipV6TVC") as! ipV6TVC
        cell.selectionStyle = .none
        cell.lbIp.text = ip?[indexPath.row].ip
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    var ipType: String?
    var ip: [IPModel]?
    var ipV4: String = ""

    @IBOutlet weak var tbv: UITableView!
    @IBOutlet weak var stact2: UIStackView!
    @IBOutlet weak var lblIpGateway: UILabel!
    @IBOutlet weak var lblIpDevice: UILabel!
    @IBOutlet weak var lblIpType: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
            lblIpType.text = ipType
            tbv.register(UINib(nibName: "ipV6TVC", bundle: nil), forCellReuseIdentifier: "ipV6TVC")
            tbv.rowHeight = 30
            tbv.estimatedRowHeight = UITableView.automaticDimension
            tbv.delegate = self
            tbv.dataSource = self
            self.tbv.separatorColor = .clear
 
    }
    
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    let name: String = String(cString: (interface!.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }

    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension Array where Element: Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            guard !uniqueValues.contains(item) else { return }
            uniqueValues.append(item)
        }
        return uniqueValues
    }
}
