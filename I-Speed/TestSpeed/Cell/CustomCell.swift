//
//  CustomCell.swift
//  I-Speed
//
//  Created by Le Hung on 4/9/22.
//

import UIKit

class CustomCell: UICollectionViewCell {

    @IBOutlet weak var imageGuide: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
