//
//  ContentCell.swift
//  I-Speed
//
//  Created by Le Hung on 3/19/21.
//

import UIKit

class ContentCell: UITableViewCell {

    @IBOutlet weak var typeConnect: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var download: UILabel!
    @IBOutlet weak var upload: UILabel!
    
    @IBOutlet weak var iconNetwork: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
