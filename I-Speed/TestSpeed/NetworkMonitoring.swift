//
//  NetworkMonitoring.swift
//  I-Speed
//
//  Created by mac on 10/8/24.
//
import Foundation
import Network

class NetworkMonitoring {
    static let shared = NetworkMonitoring()
    
    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue.global(qos: .background)
    
    var isConnected: Bool = false
    var interfaceType: NWInterface.InterfaceType = .other
    var isExpensive: Bool = false
    
    // Các closure để lắng nghe sự thay đổi
    var onStatusChange: ((Bool, NWInterface.InterfaceType, Bool) -> Void)?
    
    private init() {
        monitor.pathUpdateHandler = { [weak self] path in
            self?.isConnected = path.status == .satisfied
            self?.isExpensive = path.isExpensive
            
            if path.usesInterfaceType(.wifi) {
                self?.interfaceType = .wifi
            } else if path.usesInterfaceType(.cellular) {
                self?.interfaceType = .cellular
            } else if path.usesInterfaceType(.wiredEthernet) {
                self?.interfaceType = .wiredEthernet
            } else {
                self?.interfaceType = .other
            }
            
            // Gọi closure để thông báo về sự thay đổi
            self?.onStatusChange?(self?.isConnected ?? false, self?.interfaceType ?? .other, self?.isExpensive ?? false)
        }
        
        monitor.start(queue: queue)
    }
    
    deinit {
        monitor.cancel()
    }
}
