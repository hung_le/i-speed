//
//  LocalTestResultVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/19/21.
//

import UIKit
import CoreData

class LocalTestResultVC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            // Fallback on earlier versions
            return .default
        }
    }
    
    var testResult: [NSManagedObject] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var managedObjectContext:NSManagedObjectContext!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "ContentCell", bundle: nil), forCellReuseIdentifier: "ContentCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //setGradientBackground()
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let container = NSPersistentContainer(name: "TestResult")
        let description = NSPersistentStoreDescription()
        description.shouldMigrateStoreAutomatically = true
        description.shouldInferMappingModelAutomatically = true
        container.persistentStoreDescriptions = [description]
        
        managedObjectContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "TestResult")
                let idDes = NSSortDescriptor(key: "dateCreate", ascending: true)
                //print("idDes: \(idDes)")
                fetchRequest.fetchLimit = 50
                fetchRequest.sortDescriptors = [idDes]
        
        do {
            testResult = try managedObjectContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        testResult.reverse()
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.reloadRows(at: [selectedIndexPath], with: .automatic)
        }
    }
    
    func delete(entityName: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try appDelegate.persistentContainer.viewContext.execute(deleteRequest)
        } catch let error as NSError {
            debugPrint(error)
        }
    }
    
    @IBAction func deleteAllData(_ sender: Any) {
        let alert = UIAlertController(title: "Xoá bản ghi", message: "Toàn bộ bản ghi sẽ được xoá khỏi lịch sử đo", preferredStyle: UIAlertController.Style.alert)
        let agree = UIAlertAction(title: "Đồng ý", style: .default) {[weak self] (_) -> Void in
            self?.delete(entityName: "TestResult")
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let container = NSPersistentContainer(name: "TestResult")
            let description = NSPersistentStoreDescription()
            description.shouldMigrateStoreAutomatically = true
            description.shouldInferMappingModelAutomatically = true
            container.persistentStoreDescriptions = [description]
            
            self?.managedObjectContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "TestResult")
            do {
                self?.testResult = try self?.managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.navigationController?.popViewController(animated: true)
                }
//            }
        }
        let degree = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(agree)
        alert.addAction(degree)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

extension LocalTestResultVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var testResultSS: [NSManagedObject] = []
//        if testResult.count > 50 {
//            testResultSS = testResult.suffix(50)
//        } else {
//            testResultSS = testResult
//        }
//        return testResultSS.count
        return testResult.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var testResultSS: [NSManagedObject] = []
//        if testResult.count > 51 {
//            testResultSS = testResult.enumerated().compactMap{ $0 < 50 ? $1 : nil }
//        } else {
//            testResultSS = testResult
//        }
        let testResults = testResult[indexPath.row]
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentCell", for: indexPath) as! ContentCell
        cell.selectionStyle = .none
        if let text = testResults.value(forKeyPath: "typeConnect") as? String {
            if text == "LTE" {
                cell.iconNetwork.image = UIImage(named: "LTE")
            }else if text == "3G" {
                cell.iconNetwork.image = UIImage(named: "3G")
            }else if text == "WIFI" {
                cell.iconNetwork.image = UIImage(named: "icon-wifi-2")
            }else if text == "5G" {
                cell.iconNetwork.image = UIImage(named: "5G")
            }else if text == "2G" {
                cell.iconNetwork.image = UIImage(named: "2G")
            }
        }
        cell.typeConnect.text = testResults.value(forKeyPath: "time") as? String
        if let down = testResults.value(forKeyPath: "download") as? Double, let up = testResults.value(forKeyPath: "upload") as? Double {
            cell.download.text = String(format: "%.1f", locale: Locale(identifier: "de"), down)
            cell.upload.text = String(format: "%.1f", locale: Locale(identifier: "de"), up)
        }
        //        cell.download.text = String(format:"%.1f",(testResults.value(forKeyPath: "download") as? Double)!)
        //        cell.upload.text = String(format:"%.1f",(testResults.value(forKeyPath: "upload") as? Double)!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailResultViewController()
        vc.testResultDetail = testResult[indexPath.row]
        vc.managedObjectContext = self.managedObjectContext
        //vc.modalPresentationStyle = .overFullScreen
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension String {
    func stringTodate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension NSDate {
    
    dynamic var nextOccurrence : NSDate {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day], from: self as Date)
        return calendar.nextDate(after: Date(), matching: components, matchingPolicy: .nextTime)! as NSDate
    }
}
