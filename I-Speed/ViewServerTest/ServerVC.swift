//
//  ServerVC.swift
//  I-Speed
//
//  Created by Le Hung on 3/26/21.
//

import UIKit

protocol SendDataDelegate {
    func sendData(pointCheck: PointCheckModel)
}

protocol SendDataDelegateToTest {
    func sendData(pointCheck: PointCheckModel)
}

class ServerVC: UIViewController, UISearchBarDelegate {
    
    var delegate: SendDataDelegate!
    var delegateTest: SendDataDelegateToTest!
    var pointCheck: [PointCheckModel]?
    var id: Int?
    var filteredData: [PointCheckModel]?
    var point: Point?
    var getPoint = Point()
    var isDraw = true
    var searching = false
    
    
    var fillData: [PointCheckModel]?
    var rowToSelect: IndexPath!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewBorder: UIView!
    
    
    override func viewDidLoad() {
        tableView.layer.cornerRadius = 20
        tableView.clipsToBounds = true
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ServerCell", bundle: nil), forCellReuseIdentifier: "ServerCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        searchBar.delegate = self
        self.searchBar.searchBarStyle = UISearchBar.Style.prominent
        self.searchBar.isTranslucent = false
        textFieldInsideSearchBar?.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.01960784314, blue: 0.2235294118, alpha: 1)
        textFieldInsideSearchBar?.layer.borderWidth = 1
        textFieldInsideSearchBar?.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textFieldInsideSearchBar?.layer.cornerRadius = 18
        self.searchBar.barTintColor = #colorLiteral(red: 0.1490196078, green: 0.01960784314, blue: 0.2235294118, alpha: 1)
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.leftView?.tintColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        filteredData = pointCheck
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func applyFilters(textSearched: String) {
        
        //        filteredData = pointCheck?.filter({ item -> Bool in
        //            return (item.name?.lowercased().hasPrefix(textSearched.lowercased()) ?? false)
        //        })
        //        DispatchQueue.main.async {
        //            self.tableView.reloadData()
        //        }
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: (55/255.0), green: (19/255.0), blue: (141/255.0), alpha: 1.0).cgColor
        let colorBottom = UIColor(red: (164/255.0), green: (0/255.0), blue: (192/255.0), alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension ServerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section  == 0 ) {
            if(filteredData?.count ?? 0 > 2) {
                return 2
            } else if (filteredData?.count ?? 0 == 1) {
                return 1
            } else {
                return 0
            }
        } else {
            return filteredData?.count ?? 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
        func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//            if (isDraw) {
//                for i in 0..<2 {
//                    if (i == 0) {
//                        let frameTable = tableView.rect(forSection: i)
//                        let frame = CGRect(x: frameTable.origin.x, y: frameTable.origin.y + 40, width: frameTable.width, height: frameTable.height - 40)
//                        let imgView = UIImageView(frame: frame)
//                        imgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05196412851)
//                        imgView.layer.cornerRadius = 20
//                        tableView.insertSubview(imgView, belowSubview: tableView)
//                    } else {
//                        let frameTable = tableView.rect(forSection: i)
//                        let frame = CGRect(x: frameTable.origin.x, y: frameTable.origin.y + 40, width: frameTable.width, height: frameTable.height)
//                        let imgView = UIImageView(frame: frame)
//                        imgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05196412851)
//                        imgView.layer.cornerRadius = 20
//                        tableView.insertSubview(imgView, belowSubview: tableView)
//                    }
//
//                }
//                isDraw = false
//
//            }
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (isDraw) {
            for i in 0..<2 {
                if (i == 0) {
                    let frameTable = tableView.rect(forSection: i)
                    let frame = CGRect(x: frameTable.origin.x, y: frameTable.origin.y + 40, width: frameTable.width, height: frameTable.height - 40)
                    let imgView = UIImageView(frame: frame)
                    imgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05196412851)
                    imgView.layer.cornerRadius = 20
                    tableView.insertSubview(imgView, belowSubview: tableView)
                } else {
                    let frameTable = tableView.rect(forSection: i)
                    let frame = CGRect(x: frameTable.origin.x, y: frameTable.origin.y + 40, width: frameTable.width, height: frameTable.height + 800)
                    let imgView = UIImageView(frame: frame)
                    imgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.05196412851)
                    imgView.layer.cornerRadius = 20
                    tableView.insertSubview(imgView, belowSubview: tableView)
                }
                
            }
            isDraw = false
            
        }
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        let newlabel = UILabel()
        //206-250
        
        newlabel.textColor = UIColor.white
        newlabel.textAlignment = .left
        newlabel.font = UIFont.init(name: "Roboto", size: 16.0)
        newlabel.adjustsFontSizeToFitWidth = true
        newlabel.text = newlabel.text?.capitalized
        switch section {
        case 0:
            newlabel.text = "Lựa chọn tốt nhất"
        case 1:
            newlabel.text = "Tất cả điểm đo"
        default:
            newlabel.text = ""
        }
        
        headerView.addSubview(newlabel)
        
        newlabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 6.0))
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 20.0))
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0))
        headerView.addConstraint(NSLayoutConstraint(item: newlabel, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServerCell", for: indexPath) as! ServerCell
        let cacheUser: Point = LocalStorageManager().getUser()
        print(cacheUser)
        if(cacheUser.name != nil && filteredData?[indexPath.row].name == cacheUser.name) {
            cell.nameServer.textColor = #colorLiteral(red: 0.8509803922, green: 0.4352941176, blue: 1, alpha: 1)
        } else {
            cell.nameServer.textColor = .white
        }
        cell.selectionStyle = .none
            cell.nameServer.text = filteredData?[indexPath.row].name
            cell.cityServer.text = filteredData?[indexPath.row].city
            cell.distanceServer.text = filteredData?[indexPath.row].distance
  
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LocalStorageManager().removeUser()
        var product: Point = Point()
        product.id = filteredData?[indexPath.row].id
        product.name = filteredData?[indexPath.row].name
        product.city = filteredData?[indexPath.row].city
        product.distance = filteredData?[indexPath.row].distance
        product.baseUrl = filteredData?[indexPath.row].baseUrl
        product.downloadUrl = filteredData?[indexPath.row].downloadUrl
        product.uploadUrl = filteredData?[indexPath.row].uploadUrl
        product.pingUrl = filteredData?[indexPath.row].pingUrl
        product.pingValue = filteredData?[indexPath.row].pingValue
        
        LocalStorageManager().saveUser(user: product)
        if let point = filteredData?[indexPath.row] {
            if self.id == 1 {
                self.delegate?.sendData(pointCheck: point)
            } else if self.id == 2 {
                self.delegateTest.sendData(pointCheck: point)
            }
        }
        
        print("rowSelect1: \(product)")
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.lowercased().count == 0 {
            self.filteredData?.removeAll()
            self.filteredData = self.pointCheck
            self.tableView.reloadData()
            return
        }
        self.filteredData?.removeAll()
        
        filteredData = pointCheck?.filter({ (user) -> Bool in
            
            if user.name?.lowercased().range(of:searchText.lowercased()) != nil {
                searching = true
                return true
            }
            return false
        })
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
        }
        
        
    }
    
}

class LocalStorageManager {
    
    public func saveUser(user: Point) {
        do {
            
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(user)
            let json = String(data: jsonData, encoding: .utf8) ?? "{}"
            
            let defaults: UserDefaults = UserDefaults.standard
            defaults.set(json, forKey: "user")
            defaults.synchronize()
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    public func getUser() -> Point {
        do {
            if (UserDefaults.standard.object(forKey: "user") == nil) {
                return Point()
            } else {
                let json = UserDefaults.standard.string(forKey: "user") ?? "{}"
                
                let jsonDecoder = JSONDecoder()
                guard let jsonData = json.data(using: .utf8) else {
                    return Point()
                }
                
                let user: Point = try jsonDecoder.decode(Point.self, from: jsonData)
                return user
            }
        } catch {
            print(error.localizedDescription)
        }
        return Point()
    }
    
    public func removeUser() {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.removeObject(forKey: "user")
        defaults.synchronize()
    }
}






